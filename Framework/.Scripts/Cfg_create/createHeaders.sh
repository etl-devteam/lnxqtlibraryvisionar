#!/bin/bash

########################################################################################
#
# Script name:  createHeaders.sh
#
# Date:         31/01/2020
#
# Usage:        ./createHeaders.sh <LIB_NAME> <SRC_DIR>
#
# Description:  Crea in <SRC_DIR> i file .h .cc della nuova libreria.
#
# Args:
#               LIB_NAME - il nome della libreria;
#               SRC_DIR  - il path della directory in cui copiare i file generati. 
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

echo "Generazione sorgenti progetto..."

LIB_NAME=$1
SRC_DIR=$2
SRC_FILE=""

####### <nome_lib.h> ########

SRC_FILE="$SRC_DIR/${LIB_NAME,}.h"

echo "#ifndef ${LIB_NAME^^}_H" > $SRC_FILE
echo "#define ${LIB_NAME^^}_H" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "#include \"${LIB_NAME,,}_global.h\"" >> $SRC_FILE
echo "#include <QObject>" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "class ${LIB_NAME^^}SHARED_EXPORT ${LIB_NAME} : public QObject" >> $SRC_FILE
echo "{" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "	Q_OBJECT" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "	public:" >> $SRC_FILE
echo "		explicit ${LIB_NAME}(QObject *parent = nullptr);" >> $SRC_FILE
echo "};" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "#endif // ${LIB_NAME^^}_H" >> $SRC_FILE
echo "" >> $SRC_FILE

####### <nome_lib.cpp> ########

SRC_FILE="$SRC_DIR/${LIB_NAME,}.cpp"

echo "#include \"${LIB_NAME,}.h\"" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "${LIB_NAME}::${LIB_NAME}(QObject *parent) :" >> $SRC_FILE
echo "	QObject(parent)" >> $SRC_FILE
echo "{" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "}" >> $SRC_FILE

####### <nome_lib_global.h> ########

SRC_FILE="$SRC_DIR/${LIB_NAME,,}_global.h"

echo "#ifndef ${LIB_NAME^^}_GLOBAL_H" >> $SRC_FILE
echo "#define ${LIB_NAME^^}_GLOBAL_H" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "#include <QtCore/qglobal.h>" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "#if defined(${LIB_NAME^^}_LIBRARY)" >> $SRC_FILE
echo "#  define ${LIB_NAME^^}SHARED_EXPORT Q_DECL_EXPORT" >> $SRC_FILE
echo "#else" >> $SRC_FILE
echo "#  define ${LIB_NAME^^}SHARED_EXPORT Q_DECL_IMPORT" >> $SRC_FILE
echo "#endif" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "#endif // ${LIB_NAME^^}_GLOBAL_H" >> $SRC_FILE
echo "" >> $SRC_FILE

######### <description.cpp> ######
SRC_FILE="$SRC_DIR/description.cpp"

echo "/**" >> $SRC_FILE
echo "* \\mainpage Descrizione" >> $SRC_FILE
echo "*" >> $SRC_FILE
echo "*" >> $SRC_FILE
echo "* @author" >> $SRC_FILE
echo "* @date" >> $SRC_FILE
echo "*" >> $SRC_FILE
echo "*/" >> $SRC_FILE
echo "" >> $SRC_FILE

