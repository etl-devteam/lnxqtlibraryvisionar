#!/bin/bash

########################################################################################
#
# Script name: installaLibreriaCu.sh
#
# Date:         31/01/2020
#
# Usage:        ./installaLibreriaCu.sh <LIB_NAME> <EYETECHLIB_DIR>
#
# Description:  Installa sulla Control Unit i binari della libreria <LIB_NAME>
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria;
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################


LIB_NAME="$1"
EYETECHLIB_DIR="$2"
CU_ROOTFS_DIR="/home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs"

echo "Installazione della libreria $LIB_NAME nella ControlUnit..."

# Se sulla ControlUnit non è mai stata installata una libreria VisionAR,
# configura il file sistem con le directory e le variabili d'ambiente necessarie

if [ ! -d "$CU_ROOTFS_DIR/etc/VisionAR" ]
then
        mkdir -p "$CU_ROOTFS_DIR/etc/VisionAR"
fi

if [ ! -d "$CU_ROOTFS_DIR/etc/VisionAR/Init" ]
then
        mkdir -p "$CU_ROOTFS_DIR/etc/VisionAR/Init/env.d"
fi

if [ ! -f "$CU_ROOTFS_DIR/etc/VisionAR/Init/env.d/eyetechlab_env" ]
then
	echo "Configuro l'ambiente della Control Unit..."
	echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/lib/VisionAR" > "$CU_ROOTFS_DIR/etc/VisionAR/Init/env.d/eyetechlab_env"
        echo "export PATH=\$PATH:/usr/bin/VisionAR:/home/Apps" >> "$CU_ROOTFS_DIR/etc/VisionAR/Init/env.d/eyetechlab_env"
        chmod +x "$CU_ROOTFS_DIR/etc/VisionAR/Init/env.d/eyetechlab_env"
fi


# Installazione effettiva delle librerie nel filesystem della ControlUnit
cp -r root_arm/* $CU_ROOTFS_DIR

./registraInstallazioneCu.sh $LIB_NAME $EYETECHLIB_DIR $CU_ROOTFS_DIR

echo "Installazione completata."

exit 0

