QT       += widgets


TARGET = AbstractVisionARController
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += ABSTRACTVISIONARCONTROLLER_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/abstractVisionARController.cpp \
        src/description.cpp

HEADERS += \
        ../../include/VisionAR/Framework/abstractVisionARController.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

