#include "VisionAR/Framework/abstractVisionARController.h"


AbstractVisionARController::AbstractVisionARController(AbstractVisionARView* view, AbstractVisionARModel* model, QString app_relative_path) :
    QObject(),
    LauncherAdapterInterface(app_relative_path),
    ThreadObjInterface(),
    model(model),
    view(view)
{
    ThreadObjInterface::init(this);
    runThread();
    model->runThread();
}


AbstractVisionARController::~AbstractVisionARController()
{
    delete model;
    delete view;
}


void AbstractVisionARController::defaultConnections()
{
    connect(model, SIGNAL(showWindow(int)),    view, SLOT(showWindow(int)),    static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(model, SIGNAL(hideActiveWindow()), view, SLOT(hideActiveWindow()), static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(model, SIGNAL(showActiveWindow()), view, SLOT(showActiveWindow()), static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(model, SIGNAL(endApplication()),   this, SLOT(end()),              static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));

    connections();
}


void AbstractVisionARController::start()
{
    defaultConnections();
}


void AbstractVisionARController::end()
{
    endApplication();
}
