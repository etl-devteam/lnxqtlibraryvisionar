#include "VisionAR/Framework/abstractVisionARModel.h"
#include "VisionAR/Toolkit/alertsManager.h"
#include "VisionAR/Toolkit/batteryManager.h"

AlertsManager* alertsMng;
BatteryManager* batteryMng;

AbstractVisionARModel::AbstractVisionARModel(QObject *parent) :
    QObject(parent),
    ThreadObjInterface(),
    BtnsInterface(),
    TouchInterface(),
    activeAlert(0)
{
    BtnsInterface::init(this);
    TouchInterface::init(this);
    ThreadObjInterface::init(this);

}

AbstractVisionARModel::~AbstractVisionARModel(){
    alertsMng = 0;
    BatteryManager::cleanInstance();

}

void AbstractVisionARModel::start(){
    alertsMng = AlertsManager::getInstance();

    connect(this, SIGNAL(newAlert(int, QString, QString, bool)), alertsMng, SLOT(addAlert(int, QString, QString, bool)), static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(alertsMng, SIGNAL(showAlert(QWidget*)), this, SLOT(showAlert(QWidget*)), static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(alertsMng, SIGNAL(hideAlert(QString, bool)), this, SLOT(hideAlert(QString, bool)), static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));

    batteryMng = BatteryManager::getInstance();

    runModel();
}

void AbstractVisionARModel::showAlert(QWidget* alert){
    activeAlert =(VisionARAlert* ) alert;

}

void AbstractVisionARModel::hideAlert(QString id, bool value){


    if( id == batteryMng->getIdQuestionAlert() ){
        activeAlert = 0;
    }
    else if( id == batteryMng->getIdSwapAlert()){
        activeAlert = 0;

    }
    else{
        manageAlertsEnd(id, value);
        activeAlert = 0;
    }

}


void AbstractVisionARModel::backBtnPressed(){
    if(!activeAlert){
        backPressed();
    }
}

void AbstractVisionARModel::backBtnReleased(int timePressed){
    if(!activeAlert){
        backReleased(timePressed);
    }
}

void AbstractVisionARModel::okBtnPressed(){
    if(!activeAlert){
        okPressed();
    }
}

void AbstractVisionARModel::okBtnReleased(int timePressed){
    if(!activeAlert){
        okReleased(timePressed);
    }
}

void AbstractVisionARModel::forwardBtnPressed(){
    if(!activeAlert){
        forwardPressed();
    }

}

void AbstractVisionARModel::forwardBtnReleased(int timePressed){
    if(!activeAlert){
        forwardReleased(timePressed);
    }
}

void AbstractVisionARModel::backwardBtnPressed(){
    if(!activeAlert){
        backwardPressed();
    }
}

void AbstractVisionARModel::backwardBtnReleased(int timePressed){
    if(!activeAlert){
        backwardReleased(timePressed);
    }
}

void AbstractVisionARModel::touchSingleTap(){
    if(!activeAlert){
        singleTap();
    }
}

void AbstractVisionARModel::touchDoubleTap(){
    if(!activeAlert){
        doubleTap();
    }
}

void AbstractVisionARModel::touchSwipeForward(){
    if(!activeAlert){
        swipeForward();
    }
}

void AbstractVisionARModel::touchSwipeBackward(){
    if(!activeAlert){
        swipeBackward();
    }
}
