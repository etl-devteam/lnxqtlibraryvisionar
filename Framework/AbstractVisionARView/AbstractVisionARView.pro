QT       += widgets

TARGET = AbstractVisionARView
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += ABSTRACTVISIONARVIEW_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/abstractVisionARView.cpp \
        src/description.cpp

HEADERS += \
        ../../include/VisionAR/Framework/abstractVisionARView.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

