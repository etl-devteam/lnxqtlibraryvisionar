#include "VisionAR/Framework/abstractVisionARView.h"
#include "VisionAR/Toolkit/alertsManager.h"



AlertsManager* alertsMng;

AbstractVisionARView::AbstractVisionARView(QWidget *parent) :
    QMainWindow(parent),
    batteryLabel(),
    activeWindow(0),
    alert(0),
    coloreVisibile("#7fff00")
{
    alertsMng = AlertsManager::getInstance();

    connect(alertsMng, SIGNAL(showAlert(QWidget*)), this, SLOT(showAlert(QWidget*)));
    connect(alertsMng, SIGNAL(showingPriorityAlert()), this, SLOT(showingPriorityAlert()));

    this->setCentralWidget(new QWidget(this));
    this->setFixedSize(419, 138);

    this->setStyleSheet("background-color : black; color : "+coloreVisibile+";");
    this->setContentsMargins(0,1, 6, 4);

    this->centralWidget()->setStyleSheet("background-color : black; color : "+coloreVisibile+";");
    this->centralWidget()->setFixedSize(413, 133);



    windows.append(new QWidget());

    windows[0]->setStyleSheet("border: 0px;");
    windows[0]->setFixedSize(413, 133);


    QLabel *msg_label=new QLabel(windows[0]);
    QRect alertPosition(QPoint(), QSize( windows[0]->width(), windows[0]->height()));
    alertPosition.moveCenter(windows[0]->rect().center());
    msg_label->setGeometry(alertPosition);
    msg_label->setWordWrap(true);
    msg_label->setAlignment(Qt::AlignCenter);
  // msg_label->setText("empty window...");

    this->show();


    windows[0]->setParent(this->centralWidget());
    windows[0]->show();

    batteryIcons.append(new QPixmap());
    batteryIcons.append(new QPixmap());
    batteryIcons.append(new QPixmap());
    batteryIcons.append(new QPixmap());
    batteryIcons.value(0)->load("/etc/VisionAR/SystemIcons/high_battery.png");
    batteryIcons.value(1)->load("/etc/VisionAR/SystemIcons/med_high_battery.png");
    batteryIcons.value(2)->load("/etc/VisionAR/SystemIcons/med_low_battery.png");
    batteryIcons.value(3)->load("/etc/VisionAR/SystemIcons/low_battery.png");

    batteryLabel.setFixedHeight(17);
    batteryLabel.setFixedWidth(25);
    batteryLabel.move(383, 5);
    batteryLabel.setStyleSheet("border: 0px;");
    showingBattery.append(false);

}

AbstractVisionARView::~AbstractVisionARView(){

    if( alert )
        delete alert;

    for( QWidget *widget : windows ){
        delete widget;
    }

    alertsMng=0;
    AlertsManager::cleanInstance();

    for( QPixmap *icon: batteryIcons)
        delete icon;

   // delete batteryLabel;
}

void AbstractVisionARView::showWindow(int num){
    hideActiveWindow();

    if( !windows[num]->parent() ){
        windows[num]->setParent(this->centralWidget());
    }

    activeWindow = num;

    if(!alert){
        if(showingBattery[num]){

            batteryLabel.setParent(windows[num]);
            batteryLabel.setPixmap(batteryIcons.value(1)->scaled(batteryLabel.size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));

        }


        windows[num]->show();
    }

}

void AbstractVisionARView::showActiveWindow(){
    windows[activeWindow]->show();
}

void AbstractVisionARView::hideActiveWindow(){
    windows[activeWindow]->hide();

}

void AbstractVisionARView::showAlert(QWidget *alert){
    this->alert = alert;

    windows[activeWindow]->hide();
    this->alert->setParent(this->centralWidget());
    ((VisionARAlert*) this->alert)->showAlert(5000);

    connect(this->alert, SIGNAL(hideAlert(bool)), this, SLOT(hideAlert(bool)));

}


void AbstractVisionARView::hideAlert(bool value){

    alert->hide();

    if( !windows[activeWindow]->parent() )
        windows[activeWindow]->setParent(this->centralWidget());
    windows[activeWindow]->show();   
    QString alertId = ((VisionARAlert*) alert)->getId();
    delete alert;
    alert = 0;

    alertsMng->alertHidden( alertId, value);

}

void AbstractVisionARView::showingPriorityAlert(){

    if(alert){
        alert->hide();
        delete alert;
        alert = 0;
    }
}


