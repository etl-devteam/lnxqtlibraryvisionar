#!/bin/bash

########################################################################################
#
# Script name:  eyetechlib.sh
#
# Date:		31/01/2020
#
# Usage:	./eyetechlib.sh PARAM [LIB_NAME] [ARCH]
#
# Description:  Manager per la gestione delle librerie Eyetechlab.
#		Permette di creare un progetto libreria, la sua installazione
#		sulla macchina host o sulla Control Unit, la disinstallazione
#		dalla macchina host o dalla Control Unit.
#
# Args:
#		PARAM 	 - permette di specificare una funzionalità
#			   ( leggere la funzione help_menu per i possibili valori );
#		LIB_NAME - il nome della libreria;
#		ARCH	 - arm (default) o x86_64;
#
# Author: 	Nicola Pancheri
#
# Email: 	nicola.pancheri@akronos-tech.it
#
########################################################################################


EYETECHLIB_DIR="$PWD"
SCRIPTS_DIR="$EYETECHLIB_DIR/.Scripts"
DEFAULT_LIBS_FILE="$SCRIPTS_DIR/default_libs.log"
ARCH=""
EXIT_STATUS=0 # Dove registro l'exit status dei vari sottoscript

#Funzione che stampa il menu di help
function help_menu {
        echo
        echo "Eyetechlib: manager per la gestione delle librerie Eyetechlab per VisionAr"
        echo
        echo "Uso: ./eyetechlib.sh PARAM [LIB_NAME] [ARCH]"
        echo
        echo "PARAM:"
        echo "-c <LIB_NAME>: crea un progetto con nome <LIB_NAME>"
        echo "-h: stampa questo menu"
        echo "-icu installa sulla ControlUnit le DEF_LIBRARIES"
        echo "-ucu disinstalla dalla ControlUnit le DEF_LIBRARIES"
        echo "-ipc [ARCH] installa sul computer host le DEF_LIBRARIES compilate per arm o x86_64 a seconda di ARCH"
        echo "-upc disinstalla dal computer host le DEF_LIBRARIES"
        echo "-icus <LIB_NAME> installa sulla ControlUnit <LIB_NAME>"
        echo "-ucus <LIB_NAME> disinstalla dalla ControlUnit <LIB_NAME>"
        echo "-ipcs <LIB_NAME> [ARCH] installa sul computer host <LIB_NAME> compilata per arm o x86_64 a seconda di ARCH"
        echo "-upcs <LIB_NAME> disinstalla dal computer host le <LIB_NAME>"
        echo
        echo "DEF_LIBRARIES: librerie base Eyetechlab specificate in .Sources/default_libs.log"
        echo
        echo "ARCH: "
        echo "arm (default)"
        echo "x86_64"
        echo

}

function for_each_default_lib {
	for LIBRARY in $(cat $DEFAULT_LIBS_FILE)
	do
		cd $EYETECHLIB_DIR/$LIBRARY &> /dev/null
		STATUS=$?

		if [ $STATUS == 0 ] #se il cd è andato a buon fine per such not file or directory
		then
			cp $SCRIPTS_DIR/$1 .
			chmod +x $1

			./$1 $LIBRARY $EYETECHLIB_DIR $2 $3 
		
			EXIT_STATUS=$?
			rm $1
			cd $EYETECHLIB_DIR
			echo #Lascio uno spazio vuoto nella console
		fi
	done
}

# Controllo parametro <ARCH>
function verify_arch {
	if [ "$1" == "" ] || [ "$1" == "arm" ]
	then
        	ARCH="arm"
	elif [ "$1" == "x86_64" ]
	then
        	ARCH="x86_64"

	else
        	echo "Errore: parametro <ARCH> errato."
        	help_menu
        	exit 1
	fi

}

if [ "$1" == "-h" ] || [ "$1" == "" ]
then
        help_menu
        exit 0
fi

# Comandi che non prevedono <LIB_NAME>

# Installazione e cancellazione su/da ControlUnit
if [ "$1" == "-icu" ]
then
	for_each_default_lib installaLibreria.sh "cu" "arm" 
	exit $EXIT_STATUS 

elif [ "$1" == "-ucu" ]
then
	for_each_default_lib eliminaLibreria.sh "cu" false
	exit $EXIT_STATUS
fi




# Installazione e cancellazione su/da macchina host.

if [ "$1" == "-ipc" ]
then
	verify_arch $2
	for_each_default_lib installaLibreria.sh "pc" "$ARCH" 
	exit $EXIT_STATUS

elif [ "$1" == "-upc" ]
then
	for_each_default_lib eliminaLibreria.sh "pc" false
	exit $EXIT_STATUS
fi


LIB_NAME="$2"

# Rendo di default maiuscola la prima lettera del nome
LIB_NAME=${LIB_NAME^}

# Controllo che l'ultimo carattere del nome non sia uno slash per 
# evitare di dover gestire la cosa per ogni funzionalità.

LAST_CHAR=${LIB_NAME: -1}

if [ "$LAST_CHAR" == "/" ]
then
        echo "Eliminazione carattere di slash nel nome $LIB_NAME..."

        # wc -m sul mio pc mi ritorna sempre un carattere in più, motivo del -1
        NUM_OF_CHAR=$(($(echo "$LIB_NAME" | wc -m )-1)) 
        
        CHAR_TO_CONSIDER=$(($NUM_OF_CHAR-1))
        LIB_NAME=${LIB_NAME:0:$CHAR_TO_CONSIDER}

fi

# Comandi a cui serve il parametro <LIB_NAME>

if [ "$1" == "-c" ]
then
	# Lancio script per la creazione di un progetto
        echo "Creazione nuova progetto \"$LIB_NAME\"..."
        cp .Scripts/nuovaLibreria.sh .
        chmod +x nuovaLibreria.sh
        ./nuovaLibreria.sh $LIB_NAME $EYETECHLIB_DIR
        rm nuovaLibreria.sh
        echo "Progetto creato."

	exit 0

elif [ "$1" == "-icus" ]
then
	cd $EYETECHLIB_DIR/$LIB_NAME
	cp $SCRIPTS_DIR/installaLibreria.sh .
	chmod +x installaLibreria.sh

	./installaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "cu" $ARCH 
	EXIT_STATUS=$?

	rm installaLibreria.sh 
	cd $EYETECHLIB_DIR
	exit $EXIT_STATUS

elif [ "$1" == "-ucus" ]
then
	cd $EYETECHLIB_DIR/$LIB_NAME
	cp $SCRIPTS_DIR/eliminaLibreria.sh .
	chmod +x eliminaLibreria.sh

	./eliminaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "cu" false 
	EXIT_STATUS=$?

	rm eliminaLibreria.sh 
	cd $EYETECHLIB_DIR
	exit $EXIT_STATUS

elif [ "$1" == "-ipcs" ]
then
	verify_arch $3

	cd $EYETECHLIB_DIR/$LIB_NAME
	cp $SCRIPTS_DIR/installaLibreria.sh .
	chmod +x installaLibreria.sh

	./installaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "pc" $ARCH 
	EXIT_STATUS=$?

	rm installaLibreria.sh 
	cd $EYETECHLIB_DIR
	exit $EXIT_STATUS

elif [ "$1" == "-upcs" ]
then
	cd $EYETECHLIB_DIR/$LIB_NAME
	cp $SCRIPTS_DIR/eliminaLibreria.sh .
	chmod +x eliminaLibreria.sh

	./eliminaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "pc" false 
	EXIT_STATUS=$?

	rm eliminaLibreria.sh 
	cd $EYETECHLIB_DIR
	exit $EXIT_STATUS
fi

