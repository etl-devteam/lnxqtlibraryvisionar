    TEMPLATE      = subdirs
SUBDIRS       = \
		Framework/AbstractVisionARController \
		Framework/AbstractVisionARModel \
		Framework/AbstractVisionARView \
		Toolkit/AlertsManager \
		Toolkit/BatteryManager \
		Toolkit/BtnsInterface \
		Toolkit/LauncherAdapterInterface \
		Toolkit/ThreadObjInterface \
		Toolkit/TouchInterface \
		ToolkitDep/BtnsEventGen2 \
		ToolkitDep/GlassesCheck \
		ToolkitDep/TouchEventsGen2 \
		ToolkitDep/VisionARAlertFactory
