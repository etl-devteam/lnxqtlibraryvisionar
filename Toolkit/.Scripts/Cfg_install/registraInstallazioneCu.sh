#!/bin/bash

########################################################################################
#
# Script name:  registraInstallazioneCu.sh
#
# Date:         31/01/2020
#
# Usage:        ./registraInstallazioneCu.sh <LIB_NAME> <EYETECHLIB_DIR> <CU_ROOTFS_DIR> 
#
# Description:  Registra nome e path dei file che sono stati copiati nel filesystem
#		della Control Unit per permettere poi di indentificare agevolmente 
#		le librerie installate e disintallarle all'occorrenza.
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria
#		CU_ROOTFS_DIR  - path in cui si trova la rootfs del Cdk              
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
CU_ROOTFS_DIR="$3"
REPO_DIR="$EYETECHLIB_DIR/.Repo/CU/$LIB_NAME"
FILES_LOG="$REPO_DIR/files.log" # si inserisce il path assoluto dei files copiati nel filesystem della ControlUnit

echo "Registrazione dei files installati sulla Control Unit..."

mkdir $REPO_DIR

# Registro tutti i file .so installati nel filesystem
ls root_arm/usr/lib/VisionAR > tmp

for file in $(cat tmp)
do
	echo "$CU_ROOTFS_DIR/usr/lib/VisionAR/$file" >> $FILES_LOG
done

rm tmp

echo "Registrazione completata."

exit 0

