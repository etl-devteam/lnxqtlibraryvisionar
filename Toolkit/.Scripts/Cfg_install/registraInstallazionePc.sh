#!/bin/bash

########################################################################################
#
# Script name:  registraInstallazionePc.sh
#
# Date:         31/01/2020
#
# Usage:        ./registraInstallazionePc.sh <LIB_NAME> <EYETECHLIB_DIR> <ARCH> <INCLUDE_DIR> 
#
# Description:  Registra nome e path dei file che sono stati copiati nel filesystem
#               della macchina host per permettere poi di indentificare agevolmente 
#               le librerie installate e disinstallarle all'occorrenza.
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria
#		ARCH	       - arm o x86_64
#		INCLUDE_DIR    - path dove installare gli header (.h).
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

#
# Registra le info necessarie a poter disinstallare/aggiornare la libreria
# dalla macchina host.
#

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
ARCH="$3"
INCLUDE_DIR="$4"
REPO_DIR="$EYETECHLIB_DIR/.Repo/PC/$LIB_NAME"
FILES_LOG="$REPO_DIR/files.log" # si inserisce il path assoluto dei files copiati nel filesystem della ControlUnit
HEADER_LOG="$REPO_DIR/header.log" # vi si inseriscono le info necessarie per eliminare l'include da libVisionAR.h
ARCH_LOG="$REPO_DIR/arch.log" # registro per che tipo di architettura sono le librerie che stanno venendo installate

mkdir $REPO_DIR
touch $FILES_LOG
touch $HEADER_LOG


# Registro tutti i file .h installati nel filesystem
ls headers/VisionAR > tmp

for file in $(cat tmp)
do
        echo "$INCLUDE_DIR/VisionAR/$file" >> $FILES_LOG

done

# Registro tutti i file .so installati nel filesystem
ls root_$ARCH/usr/lib/VisionAR > tmp

for file in $(cat tmp)
do
        echo "/usr/lib/VisionAR/$file" >> $FILES_LOG

done

# Registro l'architettura per cui sono state compilate le librerie
echo "$ARCH" > $ARCH_LOG

#Registro le info necessarie a togliere l'include da libVisionAR.h
echo "$INCLUDE_DIR" >> $HEADER_LOG
echo "VisionAR/${LIB_NAME,}.h" >> $HEADER_LOG

rm tmp

exit 0


