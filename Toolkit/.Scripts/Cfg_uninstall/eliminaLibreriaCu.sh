#!/bin/bash

########################################################################################
#
# Script name: eliminaLibreriaCu.sh
#
# Date:         31/01/2020
#
# Usage:        ./eliminaLibreriaCu.sh <LIB_NAME> <EYETECHLIB_DIR>>
#
# Description:   Disinstalla dalla Control Unit la libreria <LIB_NAME>
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria;
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
REPO_DIR="$EYETECHLIB_DIR/.Repo/CU/$LIB_NAME"
FILES_LOG="files.log"

cd $REPO_DIR

for INSTALLED_FILE in $(cat $FILES_LOG)
do
        if [ -e $INSTALLED_FILE ]
        then
        	rm $INSTALLED_FILE
	fi
done

cd ..
rm -r $LIB_NAME

echo "Disinstallazione completata."

exit 0
