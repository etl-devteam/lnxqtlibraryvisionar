#!/bin/bash

########################################################################################
#
# Script name: installaLibreria.sh
#
# Date:         31/01/2020
#
# Usage:        ./installaLibreria.sh <LIB_NAME> <EYETECHLIB_DIR> <MACHINE> [ARCH]
#
# Description:  Script preliminare a quello di installazione effettiva;
#		fa controlli sui parametri, elimina eventuali
#		installazioni precedenti della libreria ed, in base ai parametri
#		passati, seleziona il giusto script di installazione da eseguire.
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#				 e le directory dei vari progetti libreria;
#		MACHINE	       - pc o cu, a seconda che si voglia installare <LIB_NAME>
#				 sulla macchina host o sulla Control Unit;
#		ARCH           - arm (default) o x86_64;
#		
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
MACHINE="$3" # pc o cu
ARCH="$4" # valido solo se $MACHINE == "pc"
USER_NAME="$USER"  
SCRIPTS_DIR="$EYETECHLIB_DIR/.Scripts"
CFG_INSTALL="$SCRIPTS_DIR/Cfg_install"
LIB_DIR="root/root_arm/usr/lib"
INCLUDE_DIR="root/headers/VisionAR/Toolkit"

if [ "$MACHINE" == "cu" ]
then
	# Disinstallo eventuali versioni precedenti della stessa libreria preinstallate

	cp $SCRIPTS_DIR/eliminaLibreria.sh .
	chmod +x eliminaLibreria.sh
	./eliminaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "cu" true
	rm eliminaLibreria.sh
	
	# Inizio la preparazione per l'installazione
	echo "Avvio installazione $LIB_NAME"
	mkdir -p $LIB_DIR
	
	cp $CFG_INSTALL/installaLibreriaCu.sh root
	cp $CFG_INSTALL/registraInstallazioneCu.sh root
	chmod +x root/installaLibreriaCu.sh
	chmod +x root/registraInstallazioneCu.sh

	if [ ! -e "lib/lib_arm/lib$LIB_NAME.so" ]
        then
                ls lib/lib_arm
                echo "Errore: prima di installare un progetto, va compilato!"
                rm -r root/* &> /dev/null
                exit 1
        fi

        cp -r lib/lib_arm $LIB_DIR/VisionAR
	cd root
	sudo ./installaLibreriaCu.sh $LIB_NAME $EYETECHLIB_DIR
	cd ..
        rm -r root/* &> /dev/null

elif [ "$MACHINE" == "pc" ]
then
	# Disinstallo eventuali versioni precedenti della stessa libreria preinstallate
	
	cp $SCRIPTS_DIR/eliminaLibreria.sh .
	chmod +x eliminaLibreria.sh
	./eliminaLibreria.sh $LIB_NAME $EYETECHLIB_DIR "pc" true
	rm eliminaLibreria.sh

	echo "Avvio installazione $LIB_NAME"
	if [ $ARCH == "arm" ]
	then
		echo "Creazone root arm..."
        
        	if [ ! -e "lib/lib_arm/lib$LIB_NAME.so" ]
        	then
                	ls lib/lib_arm
                	echo "Errore: prima di installare un progetto, va compilato!"
                	rm -r root/* &> /dev/null
                	exit 1
        	fi

	elif [ $ARCH = "x86_64" ]
	then
        	echo "Creazone root x86_64..."
        
        	if [ ! -e "lib/lib_x86_64/lib$LIB_NAME.so" ]
        	then
                	echo "Errore: prima di installare un progetto, va compilato!"
                	rm -r root/* &> /dev/null
        	        exit 1
        	fi
	fi

	# Inizio la preparazione per l'installazione
	
	LIB_DIR="root/root_$ARCH/usr/lib"
        mkdir -p $LIB_DIR
	cp $CFG_INSTALL/installaLibreriaPc.sh root
	cp $CFG_INSTALL/registraInstallazionePc.sh root
	chmod +x root/installaLibreriaPc.sh
	chmod +x root/registraInstallazionePc.sh


	# Copio i binari della libreria nella root locale
        mkdir -p $LIB_DIR/VisionAR
        cp -r lib/lib_$ARCH $LIB_DIR/VisionAR/Toolkit
	# Copio tutti gli headers nella root locale
	mkdir -p $INCLUDE_DIR
	cp src/*.h $INCLUDE_DIR

	cd root
	echo "$USER_NAME" > user.log 
	sudo ./installaLibreriaPc.sh $LIB_NAME $EYETECHLIB_DIR $ARCH 
	cd ..
        rm -r root/* &> /dev/null
fi

exit 0
