#include "VisionAR/Toolkit/alertsManager.h"
#include "VisionAR/ToolkitDep/visionARAlertFactory.h"
#include <QDir>
#include <QtXml/QDomDocument>


AlertsManager* AlertsManager::alertsMng = 0;
VisionARAlertFactory* alertsFactory;

const int FREQUENCY_CHECK_EXTERNAL_BUF=150;
const int FREQUENCY_CHECK_LOW_PRIORITY=100;
const int FREQUENCY_CHECK_HIGH_PRIORITY=50;
const QString PATH_RUN_DIR="/var/run/VisionAR";
const QString PATH_BUFFER=PATH_RUN_DIR+"/AlertGen/Buffer";


AlertsManager::AlertsManager(QObject *parent) :
    QObject(parent),
    showingAlertLowPriority(false),
    showingAlertHighPriority(false),
    actualLowPriorityAlert(0)
{
    // Controllo la presenza delle directory usate nel filesystem e in caso di assenza le creo
    QDir pathDir(PATH_RUN_DIR);
    if (!pathDir.exists()){
        pathDir.mkdir(PATH_RUN_DIR);
    }

    pathDir = QDir(PATH_RUN_DIR+"/AlertGen");
    if (!pathDir.exists()){
        pathDir.mkdir(PATH_RUN_DIR+"/AlertGen");
    }

    pathDir = QDir(PATH_BUFFER);
    if (!pathDir.exists()){
        pathDir.mkdir(PATH_BUFFER);
    }

    alertsFactory = VisionARAlertFactory::getInstance();

    timer_check_high_priority_buffer.singleShot(FREQUENCY_CHECK_HIGH_PRIORITY, this, SLOT(check_alerts_in_high_priority_buffer()));
    timer_check_low_priority_buffer.singleShot(FREQUENCY_CHECK_LOW_PRIORITY, this, SLOT(check_alerts_in_low_priority_buffer()));
    timer_check_external_buffer.singleShot(FREQUENCY_CHECK_EXTERNAL_BUF, this, SLOT(check_alerts_in_external_buffer()));
}

AlertsManager::~AlertsManager(){
    VisionARAlertFactory::cleanInstance();
}

AlertsManager* AlertsManager::getInstance(){
    if(!alertsMng){
        alertsMng = new AlertsManager();
    }

    return alertsMng;
}

void AlertsManager::cleanInstance(){
    if(alertsMng)
        delete alertsMng;
}

void AlertsManager::addAlert(int type, QString id, QString msg, bool priority){

    if( type >= alertsFactory->getMinAlertType() && type <= alertsFactory->getMaxAlertType() ){
        if( !priority ){
            low_priority_buffer.enqueue(alertsFactory->getAlert(type, id, msg));
        }
        else{
            high_priority_buffer.enqueue(alertsFactory->getAlert(type, id, msg));
        }
    }
}

void AlertsManager::check_alerts_in_low_priority_buffer(){
    if(!showingAlertLowPriority && !showingAlertHighPriority && !low_priority_buffer.isEmpty() ){
        showingAlertLowPriority = true;
        VisionARAlert* tmp = low_priority_buffer.dequeue();
        actualLowPriorityAlert = new AlertToRemember(tmp->getType(), tmp->getId(), tmp->getMsg()); //mantengo una copia dell'oggetto non una referenza...
        emit showAlert(tmp);
    }

    timer_check_low_priority_buffer.singleShot(FREQUENCY_CHECK_LOW_PRIORITY, this, SLOT(check_alerts_in_low_priority_buffer()));
}

void AlertsManager::check_alerts_in_high_priority_buffer(){
    if( !showingAlertHighPriority && !high_priority_buffer.isEmpty() ){
        showingAlertHighPriority = true;
        if( showingAlertLowPriority ){
            emit showingPriorityAlert();
        }
        emit showAlert(high_priority_buffer.dequeue());
    }

    timer_check_high_priority_buffer.singleShot(FREQUENCY_CHECK_HIGH_PRIORITY, this, SLOT(check_alerts_in_high_priority_buffer()));
}

void AlertsManager::check_alerts_in_external_buffer(){
    QDir buffer_dir(PATH_BUFFER);
    QStringList xml_files = buffer_dir.entryList(QStringList() << "*.xml", QDir::Files);

    foreach(QString filename, xml_files) {
        parse_xml_msg(filename);
    }

    timer_check_external_buffer.singleShot(FREQUENCY_CHECK_EXTERNAL_BUF, this, SLOT(check_alerts_in_external_buffer()));
}


void AlertsManager::parse_xml_msg(QString filename){

    QFile file(PATH_BUFFER+"/"+filename);
    QDomDocument xmlBOM;

    if (file.open(QIODevice::ReadOnly )){
        xmlBOM.setContent(&file);
        file.close();
    }

    QDomElement root=xmlBOM.documentElement();

    if( root.tagName() == "messaggio_alertgen" ){
        QDomElement child=root.firstChild().toElement();

        if( child.tagName() == "testo" ){
            switch(root.attribute("type").toInt()){
                case -1: //ALERT FILO
                    if(root.attribute("app_name") == "check_level_battery"){

                        high_priority_buffer.enqueue(alertsFactory->getAlert(-1, "check_level_battery", child.text()));
                    }
                    break;
                case 0:
                    high_priority_buffer.enqueue(alertsFactory->getAlert(0, root.attribute("app_name"), child.text()));

                    break;

            }
        }

    }

    file.remove();
}

void AlertsManager::alertHidden(QString id, bool value){

    if( showingAlertHighPriority){

        emit hideAlert(id, value);
        
        //Prima di rimostrare l'eventuale non priority alert nascosto svuota il buffer con priorita'
        if(high_priority_buffer.isEmpty()){

            if( showingAlertLowPriority ){ //rimostra l'alert che aveva sostituto
                emit showAlert(alertsFactory->getAlert(actualLowPriorityAlert->getType(), actualLowPriorityAlert->getId(), actualLowPriorityAlert->getMsg()));
            }
            showingAlertHighPriority = false;
        }
        else{
            emit showAlert(high_priority_buffer.dequeue());
        }

    }
    else if(showingAlertLowPriority){
        showingAlertLowPriority = false;
        delete  actualLowPriorityAlert;
        actualLowPriorityAlert = 0;
        emit hideAlert(id, value);
    }

}
