QT       += widgets
QT       -= gui

TARGET = BatteryManager
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += BATTERYMANAGER_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/batteryManager.cpp \
        src/description.cpp

HEADERS += \
        ../../include/VisionAR/Toolkit/batteryManager.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

