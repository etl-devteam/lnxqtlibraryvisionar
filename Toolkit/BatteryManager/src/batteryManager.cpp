#include "VisionAR/Toolkit/batteryManager.h"
#include "VisionAR/ToolkitDep/btnsEventGen.h"
#include "VisionAR/Toolkit/alertsManager.h"
#include <QFile>
#include <QTextStream>
#include <QProcess>

BtnsEventGen* btnsEvGen;
AlertsManager* alertsMng;

BatteryManager* BatteryManager::batteryMng = 0;

const int FREQ_TIMER = 5000;
const QString LOCAL_BATTERY="/sys/class/dinema/local_battery";

BatteryManager::BatteryManager(QObject *parent) :
    QObject(parent),
    timer(new QTimer(this)),
    timer_elapsed(false),
    idQuestionAlert("BatteryManagerQuestionAlert"),
    idSwapAlert("BatteryManagerSwapAlert")
{
    btnsEvGen = BtnsEventGen::getInstance();

    btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(backBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(backBtnReleased(int)), false);

    alertsMng = AlertsManager::getInstance();
    connect(this, SIGNAL(newAlert(int, QString, QString, bool)), alertsMng, SLOT(addAlert(int, QString, QString, bool)), static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
    connect(alertsMng, SIGNAL(hideAlert(QString, bool)), this, SLOT(hideAlert(QString, bool)), static_cast<Qt::ConnectionType>(Qt::BlockingQueuedConnection));

    timer->setInterval(FREQ_TIMER);
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(startCheckSwitchBattery()));

}

BatteryManager::~BatteryManager(){
    if(timer)
        delete timer;
}

BatteryManager* BatteryManager::getInstance(){
    if(!batteryMng){
        batteryMng = new BatteryManager();
    }

    return batteryMng;
}

void BatteryManager::cleanInstance(){
    if( batteryMng )
        delete batteryMng;
}

void BatteryManager::backBtnPressed(){

    if( !timer_elapsed ){
        timer->start();
    }
}

void BatteryManager::backBtnReleased(int /* time */){

    if(!timer_elapsed){
        timer->stop();
    }

}

void BatteryManager::startCheckSwitchBattery(){   

    timer_elapsed = true;
    emit newAlert(1, idQuestionAlert, "Do you want to switch battery?", true);


}

void BatteryManager::hideAlert(QString id, bool value ){

    if( id == idQuestionAlert ){

        if(value){

            QFile battery(LOCAL_BATTERY);

            battery.open(QIODevice::ReadOnly | QIODevice::Text);

            QTextStream stream(&battery);

            int localBatteryVal = stream.readAll().toInt();

            battery.close();


            if( localBatteryVal == 0 ){
                QProcess shell;
                shell.execute("/bin/sh", QStringList() << "-c" << "echo 1 > "+LOCAL_BATTERY);
                shell.close();

                emit newAlert(0, idSwapAlert, "Now you can switch battery!", true);
            }
            else{
                emit newAlert(0, idSwapAlert, "It's already possible to switch battery.", true);

            }
        }
        else{ //se risponde no alla domanda "vuoi fare lo switch della batteria?"
            timer_elapsed = false;

        }
    }
    else if( id == idSwapAlert ){
        timer_elapsed = false;

    }

}

QString BatteryManager::getIdSwapAlert(){
    return idSwapAlert;
}

QString BatteryManager::getIdQuestionAlert(){
    return idQuestionAlert;
}

