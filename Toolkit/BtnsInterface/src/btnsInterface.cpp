#include "VisionAR/Toolkit/btnsInterface.h"
#include "VisionAR/ToolkitDep/btnsEventGen.h"

BtnsEventGen* btnsEvGen;

BtnsInterface::BtnsInterface()
{

    btnsEvGen = BtnsEventGen::getInstance();

}

void BtnsInterface::init(QObject *parent){
    btnsEvGen->connectSlotsToBtnSignals(4, parent, SLOT(backBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(2, parent, SLOT(okBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(1, parent, SLOT(backwardBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(3, parent, SLOT(forwardBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(4, parent, SLOT(backBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(2, parent, SLOT(okBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(1, parent, SLOT(backwardBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(3, parent, SLOT(forwardBtnReleased(int)), false);
}

BtnsInterface::~BtnsInterface(){
    BtnsEventGen::cleanInstance();
}

void BtnsInterface::connectSlotsToBtnSignals(int btn, const QObject *receiver, const char *member, bool pressed){
    btnsEvGen->connectSlotsToBtnSignals(btn, receiver, member, pressed);
}
