#include "VisionAR/Toolkit/launcherAdapterInterface.h"
#include <QProcess>
#include <QFile>
#include "VisionAR/ToolkitDep/glassesCheck.h"

const QString LOG_DIR="/var/log/VisionAR";
const QString LAUNCHER="/home/dinex/bin/VisionARLauncher";

GlassesCheck *glassesCheck;


LauncherAdapterInterface::LauncherAdapterInterface(QString app_relative_path)
{
    if (app_relative_path != nullptr)
    {
        // Code for copy the current application in a folder used for re-launch the closed application in case of release of glasses cable

        QString cmd_restart = "cp  /home/dinex/bin/apps/"+app_relative_path+" /home/dinex/bin/app_running";      // Name of application to write in a temporary file contained in a folder ('app_running')
        QProcess shell_restart;
        shell_restart.execute("/bin/sh", {"-c", cmd_restart});
        shell_restart.execute("/bin/sh", {"-c", "echo " + QString::number(QApplication::applicationPid()) + " > " + LOG_DIR + "/pid"});
        shell_restart.close();

        glassesCheck = GlassesCheck::getInstance(QApplication::applicationPid());

        // Turn on blue LED to indicate that the app is running. Turn off when application is closed
        QFile led_battery_file("/sys/class/dinema/led_RGB");
        led_battery_file.open(QIODevice::WriteOnly| QIODevice::Text);
        led_battery_file.write("4");
        led_battery_file.close();
    }
}


LauncherAdapterInterface::~LauncherAdapterInterface()
{
}


void LauncherAdapterInterface::endApplication()
{
    endActions();
    QFile led_battery_file("/sys/class/dinema/led_RGB");
    led_battery_file.open(QIODevice::WriteOnly| QIODevice::Text);
    led_battery_file.write("0");
    led_battery_file.close();

    QProcess shell;
    shell.execute("/bin/sh", {"-c", "rm  /home/dinex/bin/app_running/*"});
    shell.execute("/bin/sh", {"-c", LAUNCHER + " &"});
    shell.close();

    qApp->exit();
}


void LauncherAdapterInterface::startCheckingGlasses()
{
    GlassesCheck::getInstance(QApplication::applicationPid());
}
