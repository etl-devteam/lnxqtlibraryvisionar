QT       += widgets

TARGET = ThreadObjInterface
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += THREADOBJINTERFACE_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/description.cpp \
        src/threadobjinterface.cpp

HEADERS += \
        ../../include/VisionAR/Toolkit/threadobjinterface.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

