#include "VisionAR/Toolkit/threadObjInterface.h"

ThreadObjInterface::ThreadObjInterface(){

}

ThreadObjInterface::~ThreadObjInterface(){

}

void ThreadObjInterface::init(QObject *superObj ){
    superObj->moveToThread(&thread);
    QObject::connect(&thread, SIGNAL(started()), superObj, SLOT(start()));
    QObject::connect(&thread, SIGNAL(finished()), superObj, SLOT(deleteLater()));
}

void ThreadObjInterface::runThread(){
    thread.start();
}

void ThreadObjInterface::stopThread(){
    thread.quit();
    thread.wait();
}
