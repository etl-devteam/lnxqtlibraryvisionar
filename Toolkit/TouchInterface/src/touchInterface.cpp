#include "VisionAR/Toolkit/touchInterface.h"
#include "VisionAR/ToolkitDep/touchEventsGen.h"

TouchEventsGen* touchEvGen;

TouchInterface::TouchInterface()
{   
    touchEvGen = TouchEventsGen::getInstance("/dev/input/event0");
}

void TouchInterface::init(QObject *parent){
    touchEvGen->connectSlotsToTouchSignals(1, parent, SLOT(touchSingleTap()));
    touchEvGen->connectSlotsToTouchSignals(2, parent, SLOT(touchDoubleTap()));
    touchEvGen->connectSlotsToTouchSignals(3, parent, SLOT(touchSwipeForward()));
    touchEvGen->connectSlotsToTouchSignals(4, parent, SLOT(touchSwipeBackward()));
}

TouchInterface::~TouchInterface(){
    TouchEventsGen::cleanInstance();
}

void TouchInterface::connectSlotsToTouchSignals(int btn, const QObject *receiver, const char *member){
    touchEvGen->connectSlotsToTouchSignals(btn, receiver, member );
}
