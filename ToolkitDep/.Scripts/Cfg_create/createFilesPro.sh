#!/bin/bash

########################################################################################
#
# Script name:  createFilesPro.sh
#
# Date:         31/01/2020
#
# Usage:        ./createFilesPro.sh <LIB_NAME> <SRC_DIR>>
#
# Description:  Crea in <SRC_DIR> i file .pro e .pro.user della nuova libreria; 
#
# Args:
#               LIB_NAME - il nome della libreria;
#               SRC_DIR  - il path della directory in cui copiare i file generati. 
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

echo "Generazione files Qt..."

LIB_NAME=$1
SRC_DIR=$2
SRC_FILE=""

######### <nome_lib.pro> ##########

SRC_FILE="$SRC_DIR/${LIB_NAME}.pro"

echo "QT       += widgets" >> $SRC_FILE
echo "QT       -= gui" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "TARGET = ${LIB_NAME}" >> $SRC_FILE
echo "TEMPLATE = lib" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "DEFINES += ${LIB_NAME^^}_LIBRARY" >> $SRC_FILE
echo "DEFINES += QT_DEPRECATED_WARNINGS" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "SOURCES += \\" >> $SRC_FILE
echo "        ${LIB_NAME,}.cpp \\" >> $SRC_FILE
echo "        description.cpp" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "HEADERS += \\" >> $SRC_FILE
echo "        ${LIB_NAME,}.h \\" >> $SRC_FILE
echo "        ${LIB_NAME,,}_global.h" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "unix {" >> $SRC_FILE
echo "    target.path = /usr/lib/VisionAR" >> $SRC_FILE
echo "    INSTALLS += target" >> $SRC_FILE
echo "	DESTDIR = \\\"\$\$IN_PWD/../lib/lib_arm\\\"" >> $SRC_FILE
echo "" >> $SRC_FILE
echo "}" >> $SRC_FILE
echo "" >> $SRC_FILE

