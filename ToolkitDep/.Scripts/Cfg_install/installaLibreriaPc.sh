#!/bin/bash

########################################################################################
#
# Script name: installaLibreriaPc.sh
#
# Date:         31/01/2020
#
# Usage:        ./installaLibreriaPc.sh <LIB_NAME> <EYETECHLIB_DIR> [ARCH]
#
# Description:  Installa sulla macchina host i binari della libreria <LIB_NAME>
#		compilati per arm o x86_64 a seconda di $ARCH
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria
#		ARCH	       - arm o x86_64;	 
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
ARCH="$3"
USER_NAME=$(cat user.log)
USER_HOME="/home/$USER_NAME"
INCLUDE_DIR="/home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/include"
ENV_FILE=".visionarToolkitDep_env"

echo "Installazione della libreria $LIB_NAME nella macchina host..."

if [ "$ARCH" == "x86_64" ]
then
	INCLUDE_DIR="/usr/include"
fi

# Se siamo alla prima installazione di una libreria VisionAR,
# rendiamo disponibile nel sistema la locazione delle librerie condivise VisionAR 
# tramite la variabile d'ambiente LD_LIBRARY_PATH
if [ ! -f "$USER_HOME/$ENV_FILE" ]
then
        echo "Setto il file .bashrc per rendere globalmente diponibili le librerie VisionAR..."

        for CICLE in 1 2
        do
                if [ "$CICLE" == "2" ]
                then
                        USER_HOME="/root"
                fi

		echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/lib/VisionAR/ToolkitDep" > $USER_HOME/$ENV_FILE

                chmod +x $USER_HOME/$ENV_FILE

                echo "" >> $USER_HOME/.bashrc
                echo "if [ -f $ENV_FILE ]" >> $USER_HOME/.bashrc
                echo "then" >> $USER_HOME/.bashrc
                echo "  source $ENV_FILE" >> $USER_HOME/.bashrc
                echo "fi" >> $USER_HOME/.bashrc
                echo "" >> $USER_HOME/.bashrc
        done
	
	# rendo comunque disponibile in tutto il sistema lo script
	#cp $USER_HOME/$ENV_FILE /usr/bin/eyetechlab_env

        source $USER_HOME/$ENV_FILE
fi

# Installazione effettiva delle librerie nel filesystem

cp -r headers/* $INCLUDE_DIR
cp -r root_$ARCH/* /

./registraInstallazionePc.sh $LIB_NAME $EYETECHLIB_DIR $ARCH $INCLUDE_DIR

echo "Installazione completata."

exit 0

