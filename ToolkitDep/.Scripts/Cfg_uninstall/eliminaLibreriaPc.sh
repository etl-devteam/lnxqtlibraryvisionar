#!/bin/bash

########################################################################################
#
# Script name:  eliminaLibreriaPc.sh
#
# Date:         31/01/2020
#
# Usage:        ./eliminaLibreriaPc.sh <LIB_NAME> <EYETECHLIB_DIR>>
#
# Description:   Disinstalla dalla macchina host la libreria <LIB_NAME>
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria;
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################


LIB_NAME="$1"
EYETECHLIB_DIR="$2"
REPO_DIR="$EYETECHLIB_DIR/.Repo/PC/$LIB_NAME"
FILES_LOG="files.log"
HEADER_LOG="header.log" 
ARCH_LOG="arch.log" 

cd $REPO_DIR

USER_NAME=$(cat $EYETECHLIB_DIR/user.log)

for INSTALLED_FILE in $(cat $FILES_LOG)
do
        if [ -e $INSTALLED_FILE ]
        then
                rm $INSTALLED_FILE
        fi
done

# Elimino l'include della libreria dal file libVisionAR.h

FIRST_LINE=true

#Dove viene salvato il path assoluto della directory contenente il file libVisionAR.h
PATH_LIBEYETECHLAB=""
#Dove viene salvata la specifica stringa da togliere da libVisionAR.h
INCLUDE_TO_DEL=""

for LINE in $(cat header.log)
do
        if $FIRST_LINE
        then
                FIRST_LINE=false
                PATH_LIBEYETECHLAB=$LINE
        else
                INCLUDE_TO_DEL=$LINE
        fi
done

NUM_LINE_TO_DEL=$(grep -n "$INCLUDE_TO_DEL" "$PATH_LIBEYETECHLAB/libVisionAR.h" | cut -d : -f 1)

sed -e $NUM_LINE_TO_DEL"d" "$PATH_LIBEYETECHLAB/libVisionAR.h" > tmp

chmod 666 tmp
chown "$USER_NAME:$USER_NAME" tmp
mv tmp "$PATH_LIBEYETECHLAB/libVisionAR.h"


cd ..
rm -r $LIB_NAME

echo "Disinstallazione completata."

exit 0

