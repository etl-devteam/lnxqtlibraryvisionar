#!/bin/bash

########################################################################################
#
# Script name: eliminaLibreria.sh
#
# Date:         31/01/2020
#
# Usage:        ./eliminaLibreria.sh <LIB_NAME> <EYETECHLIB_DIR> <MACHINE> <UPDATE> 
#
# Description:  Script preliminare a quello di disinstallazione effettiva;
#               fa controlli sui parametri ed, in base a questi, seleziona il giusto 
#		script di disinstallazione da eseguire.
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria;
#               MACHINE        - pc o cu, a seconda che si voglia installare <LIB_NAME>
#                                sulla macchina host o sulla Control Unit;
#               UPDATE         - true se si tratta di una disinstallazione preliminare
#				 ad una nuova installazione;
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
MACHINE="$3" # pc o cu
UPDATE=$4
USER_NAME="$USER"
SCRIPTS_DIR="$EYETECHLIB_DIR/.Scripts"
CFG_UNINSTALL="$SCRIPTS_DIR/Cfg_uninstall"

#Se la libreria non è registrata come installata blocco la disinstallazione
function check_if_installed {

	if $UPDATE
	then
        	echo "Ricerca della libreria $LIB_NAME richiesta tra quelle installate..."
	fi

	if [ ! -d "$1" ]
	then
	        if $UPDATE      # Se è un'installazione/update non segnalo l'errore 
	        then
	                echo "Nuova installazione della libreria $LIB_NAME."
	                exit 0
	        else
	                echo "Errore: la libreria $LIB_NAME non e' installata."
	                exit 1
	        fi

	fi

	if $UPDATE
	then
	        echo "Cancellazione delle precedenti versioni della libreria $LIB_NAME..."
	else
	        echo "Disinstallazione della libreria $LIB_NAME..."
	fi

}



if [ "$MACHINE" == "cu" ]
then
	REPO_DIR="$EYETECHLIB_DIR/.Repo/CU/$LIB_NAME"
	
	check_if_installed $REPO_DIR

	cp $CFG_UNINSTALL/eliminaLibreriaCu.sh .
	chmod +x eliminaLibreriaCu.sh

	sudo ./eliminaLibreriaCu.sh $LIB_NAME $EYETECHLIB_DIR 

	rm eliminaLibreriaCu.sh
	exit 0

elif [ "$MACHINE" == "pc" ]
then
	REPO_DIR="$EYETECHLIB_DIR/.Repo/PC/$LIB_NAME"
	
	check_if_installed $REPO_DIR
	
	cp $CFG_UNINSTALL/eliminaLibreriaPc.sh .
	chmod +x eliminaLibreriaPc.sh
	echo "$USER_NAME" > $EYETECHLIB_DIR/user.log

	sudo ./eliminaLibreriaPc.sh $LIB_NAME $EYETECHLIB_DIR 

	rm eliminaLibreriaPc.sh
	rm $EYETECHLIB_DIR/user.log
	exit 0
fi
