#!/bin/bash

########################################################################################
#
# Script name:  nuovaLibreria.sh
#
# Date:         31/01/2020
#
# Usage:        ./nuovaLibreria.sh <LIB_NAME> <EYETECHLIB_DIR>>
#
# Description:  Crea in <EYETECHLIB_DIR> una directory denominata <LIB_NAME>
#		contenete i sorgenti e directory necessari ( e preconfigurati ) 
#		per la creazione di una nuova libreria.
#
# Args:
#               LIB_NAME       - il nome della libreria;
#               EYETECHLIB_DIR - la posizione in cui si trova lo script eyetechlib.sh
#                                e le directory dei vari progetti libreria;
#
# Author:       Nicola Pancheri
#
# Email:        nicola.pancheri@akronos-tech.it
#
########################################################################################


#
# Crea la directory di lavoro per la creazione di una nuova
# libreria, con anche una struttura base del codice della nuova
# libreria.
#

LIB_NAME="$1"
EYETECHLIB_DIR="$2"
CFG_CREATE="$EYETECHLIB_DIR/.Scripts/Cfg_create"

# Creazione albero di directory 

mkdir -p $EYETECHLIB_DIR/$LIB_NAME/root
mkdir -p $EYETECHLIB_DIR/$LIB_NAME/doc
mkdir -p $EYETECHLIB_DIR/$LIB_NAME/src/obj
mkdir -p $EYETECHLIB_DIR/$LIB_NAME/lib/lib_arm
mkdir -p $EYETECHLIB_DIR/$LIB_NAME/lib/lib_x86_64

# Creazione sorgenti 
SRC_DIR=$EYETECHLIB_DIR/$LIB_NAME/src

cp $CFG_CREATE/* .

chmod +x createHeaders.sh
chmod +x createFilesPro.sh
chmod +x cleanObjDir.sh

mv cleanObjDir.sh $SRC_DIR
source createHeaders.sh $LIB_NAME $SRC_DIR
source createFilesPro.sh $LIB_NAME $SRC_DIR

rm createHeaders.sh
rm createFilesPro.sh

#Impostazione files base doxygen
cp $EYETECHLIB_DIR/.Scripts/Doxygen/doxyconf $EYETECHLIB_DIR/$LIB_NAME

PRESENTATION_DOXYGEN="description.cpp"
echo "/**" > $PRESENTATION_DOXYGEN
echo "* \mainpage Description" >> $PRESENTATION_DOXYGEN
echo "* " >> $PRESENTATION_DOXYGEN
echo "* @author" >> $PRESENTATION_DOXYGEN
echo "* @date" >> $PRESENTATION_DOXYGEN
echo "* " >> $PRESENTATION_DOXYGEN
echo "*/" >> $PRESENTATION_DOXYGEN

mv $PRESENTATION_DOXYGEN $SRC_DIR


