/**
* \mainpage Description
* 
*	The VisionAR QT developers API's are intended for facilitate the development of applications for
*   the ControlUnit and VisionAR devices.<br/>
*   They came along with a script file called eyetechlib.sh, the use of which is descripted below:
* 
*  <i><b>
* ./eyetechlib.sh PARAM [LIB_NAME] [ARCH] <br/>
*
* PARAM:<br/>
*   <i class="tab">-c <LIB_NAME>: creates a project with name <LIB_NAME></i><br/>
*   <i class="tab">-h: prints this menu</i><br/>
*   <i class="tab">-icu: installs the DEF_LIBRARIES on to Control Unit</i><br/>
*   <i class="tab">-ucu: uninstalls the DEF_LIBRARIES from the Control Unit</i><br/>
*   <i class="tab">-ipc [ARCH]: installs the DEF_LIBRARIES compiled for ARCH on host machine</i><br/>
*   <i class="tab">-upc: uninstalls the DEF_LIBRARIES from the host machine</i><br/>
*   <i class="tab">-icus <LIB_NAME>: installs <LIB_NAME> on to the Control Unit</i><br/>
*   <i class="tab">-ucus <LIB_NAME>: uninstalls <LIB_NAME> from the Control Unit</i><br/>
*   <i class="tab">-ipcs <LIB_NAME> [ARCH]: installs <LIB_NAME> compiled for ARCH on to the host machine</i><br/>
*   <i class="tab">-upcs <LIB_NAME>: uninstalls <LIB_NAME> from the host machine</i><br/><br/>
*
* DEF_LIBRARIES: default VisionAR libraries indicated in .Sources/default_libs.log<br/>
*
* ARCH: <br/>
*   <i class="tab">arm (default)</i><br/>
*   <i class="tab">x86_64</i><br/>
* </b></i>
*
* \section arch Multi-architetture
*   This API's are compiled for two architetture: arm and x86_64.
*   You can find binaries in <LIB_NAME>/lib/lib_arm and <LIB_NAME>/lib/lib_x86_64.<br/>
*   Binaries for x86_64 are intended to be used in application design/debugging phase
*   while binaries for arm are for compile application to be run on the Control Unit.
* 
* \section install Installation
*  
* You can choose to install these libraries on the Control Unit or on the host machine;
* for the latter you can choose also which architetture you want. <br/> Finally, you can choose
* also to installs the entire set of libraries we offer (DEF_LIBRARIES) or to install a predetermined one.<br/>
* Every parameters that install start with <i>-i***</i>.<br/>
* 
* <b>NOTE</b> if you have installed our last CDK, you shouldn't need to install these libraries on the Control Unit
*   but only on your host machine for developing purposes.
* 
* \subsection def_libs The DEF_LIBRARIES
* 
*  <img src="classDiag.png"  width="70%" height="60%">
*
*  The DEF_LIBRARIES are the entire set of libraries we offer;
*  it's a set of classes composed by:
*  <ul>
*   <li>AlertGen</li>
*   <li>BtnsSimulator</li>
*   <li>BtnsEventGen</li>
*   <li>TouchEventsGen</li>
*   <li>BaseEventManager</li>
*   <li>BaseWindowVisionAR</li>
*   <li>BaseThreadsManager</li>
*  </ul>
* 
* To work properly, our enviroments requires that you extend these abstract classes:
*  - BaseEventManager
*  - BaseWindowVisionAR
*  - BaseThreadsManager
*
* The other classes are included in these abstract classes.
*
* \subsection install_def Installing all the DEF_LIBRARIES 
* 
* For installing the DEF_LIBRARIES for arm on the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -ipc</i><br/>
* 
* For installing the DEF_LIBRARIES for x86_64 on the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -ipc x86_64</i><br/>
* 
* <b>NOTE</b> if you use -ipc parameter, eyetechlib.sh will unistall all the previous
* installed libraries automatically.
*
* For installing the DEF_LIBRARIES on the Control Unit give:<br/>
*   <i class="tab">./eyetechlib.sh -icu </i><br/>
* 
* \subsection install_singles Installing a single library
* 
* You can choose to install also a single library only.<br/>
* 
* For installing the library <LIB_NAME> for arm on the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -ipcs <LIB_NAME></i><br/>
* 
* For installing the library <LIB_NAME> for x86_64 on the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -ipDEF_LIBRARIEScs <LIB_NAME> x86_64</i><br/>
* 
* For installing the library <LIB_NAME> on Control Unit give:<br/>
*   <i class="tab">./eyetechlib.sh -icus <LIB_NAME></i><br/>
* 
* \section Uninstall
* 
* As well as for Installation, you can choose to uninstall a single library  or
* all the DEF_LIBRARIES,<br/>if uninstall the libraries from the host machine or from the Control Unit.
* 
* <b>NOTE</b> For uninstall, the architetture dosen't matter.
* 
* \subsection uninstall_def Uninstall all the DEF_LIBRARIES
* 
* For uninstalling the DEF_LIBRARIES from the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -upc</i><br/>
*
* For uninstalling the DEF_LIBRARIES from the Control Unit give:<br/>
*   <i class="tab">./eyetechlib.sh -ucu </i><br/>
*  
* 
* \subsection uninstall_def Uninstall a single library
* 
* 
* For uninstalling the library <LIB_NAME> for arm on the host machine give:<br/>
*   <i class="tab">./eyetechlib.sh -upcs <LIB_NAME></i><br/>
* 
* For uninstalling the library <LIB_NAME> on Control Unit give:<br/>
*   <i class="tab">./eyetechlib.sh -ucus <LIB_NAME></i><br/>
* 
* \subsection hard_clean Host machine hard clean
* 
* if, for whatever reasons, you want to clean all the libraries ( and other files created during installation phase )
*  you can use launch the script file <b><i>hardClean.sh</i></b>.<br/> You can find it near <b><i>eyetechlib.sh</i></b>.
* 
* \section new_proj Create a new library project
* 
* With <b><i>eyetechlib.sh</i></b> you can also create a new project giving the command:<br/>
*   <i class="tab">./eyetechlib.sh -c <LIB_NAME></i><br/>
* 
* It will create a <LIB_NAME> folder structured as follow:
*   - doc/ : used by doxygen to create the documentation;
*   - lib/ : where will be putted the binaries once <LIB_NAME> sources will be compiled;
*   - root/ : to live untouched; used by installation proccess as working_dir;
*   - src/ : where reside <LIB_NAME> sources;
*   - doxyconf : doxygen configure file.
* 
* Although most of the needed changes are done by eyetechlib.sh, some of them are to do manually before compile.
*
* \subsection obj_dir Setting the object directory
* 
* For <i>object directory</i> we mean the directory where <i>Qtcreator</i> generates object files in compilation phase.<br/>
* Whatever architetture you want compile for, we'll use for this purpose the directory: <i><LIB_NAME>/src/obj</i>.<br/>
* For setting it, open your project with Qtcreator and, once Qtcreator has loaded it, press Ctrl+5.<br/>
* Now, for each device listed under the section <b>Build & Run</b>, select <i>Build</i> ( it has an hammer icon ); <br/>this
* will open the page <b>Build Settings</b> for that device where you can edit the <b>Build directory</b> option
* in the <b>General</b> section.<br/>
* You have to set the <b>Build directory</b> option ( for each device ) with the absolute path to: <i><LIB_NAME>/src/obj</i>.<br/>
* 
* \subsection lib_dir Setting the architetture you want compile for
* 
* Compiling your application for arm or x86_64 requires the same passeges:
*  <ol>
*   <li> Set the destination directory of the binaries:<br/> this requires to set the DESTDIR variable in the file <LIB_NAME>.pro;
*         you have to set DESTDIR = \"$$IN_PWD/../lib/lib_arm\" or DESTDIR = \"$$IN_PWD/../lib/lib_x86_64\" based on the choosen architetture.
*   </li>
*   <li>
*       Set the device the application is destined for ( so the compiler you want to use ): <br/>this requires to press Ctrl+5
*       and, in the section <b>Build & Run</b> on your left, select the <i>Build</i> ( it has an hammer icon ) of the wanted target machine.
*   </li>
*  </ol>
* 
* \section use_libs Using VisionAR QT API's
* 
* For using our API's in your application you have to include the header libVisionAR.h:<br/>
* <i class="tab">#include <libVisionAR.h></i>
* 
* After that, the last step is to edit the file .pro of your QT project;
* <ol>
*   <li>
*     Check that the QT variable is set set like follow:<br/>
*     <i class="tab">QT  += core gui widgets xml</i>
*   </li>
*   <li>
*      Append the following instuction:<br/>
*      <i class="tab">LIBS += /usr/lib/VisionAR/*</i>
*   </li>
* </ol>
* 
* 
* 
* @author Nicola Pancheri - <nicola.pancheri@akronos-tech.it>
* @date 26/02/2020
* 
*/

