QT       += widgets
QT       -= gui

TARGET = BtnsEventGen2
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += BTNSEVENTGEN_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/btnsEventGen.cpp \
        src/description.cpp \
        src/btnsSimulator.cpp

HEADERS += \
        ../../include/VisionAR/ToolkitDep/btnsEventGen.h \
        src/btnsSimulator.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

