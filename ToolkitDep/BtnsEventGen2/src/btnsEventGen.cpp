#include "VisionAR/ToolkitDep/btnsEventGen.h"
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <QTextStream>
#include "btnsSimulator.h"

#define DEBUG false

BtnsSimulator *btnsSim;


const int FREQ_CHECK_TIMER=50;

BtnsEventGen* BtnsEventGen::btnsEvGen = 0;

/**
 * @file btnsEventGen.cpp
 * @brief BtnsEventGen class implementation
 */


BtnsEventGen::BtnsEventGen(QObject *parent) :
    QObject(parent),
    timeBackPressed(nullptr),
    timeOkPressed(nullptr),
    timeForwardPressed(nullptr),
    timeBackwardPressed(nullptr),
    valueBtnBack(0),
    valueBtnOk(0),
    valueBtnForward(0),
    valueBtnBackward(0)
{

#if DEBUG
    btnsSim = new BtnsSimulator("ricordati di inserire il device file della tastiera XD");
#else
    btnsSim = nullptr;
#endif

    if( btnsSim == nullptr ){
        path_btns_dir = "/sys/class/dinema";

    }
    else{
        path_btns_dir = btnsSim->getBtnsLocation();
    }

    btnBack = new QFile(path_btns_dir+"/"+"input_key4");
    btnOk = new QFile(path_btns_dir+"/"+"input_key2");
    btnForward = new QFile(path_btns_dir+"/"+"input_key3");
    btnBackward = new QFile(path_btns_dir+"/"+"input_key1");



    check_timer.singleShot( FREQ_CHECK_TIMER, this, SLOT(checkChangeInBtnsValue()) );
}

BtnsEventGen::~BtnsEventGen(){

    delete btnsSim;

    delete btnBack;
    delete btnOk;
    delete btnForward;
    delete btnBackward;

}

BtnsEventGen* BtnsEventGen::getInstance(){
    if( !btnsEvGen )
        btnsEvGen = new BtnsEventGen();

    return btnsEvGen;

}

void BtnsEventGen::cleanInstance(){

    if( btnsEvGen )
        delete btnsEvGen;
}

void BtnsEventGen::checkChangeInBtnsValue(){

    // Lettura valori attuali dei btns
    int tmpValueBtnBack;
    int tmpValueBtnOk;
    int tmpValueBtnForward;
    int tmpValueBtnBackward;

    btnBack->open(QIODevice::ReadOnly | QIODevice::Text);
    btnOk->open(QIODevice::ReadOnly | QIODevice::Text);
    btnForward->open(QIODevice::ReadOnly | QIODevice::Text);
    btnBackward->open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamBtnBack(btnBack);
    QTextStream streamBtnOk(btnOk);
    QTextStream streamBtnForward(btnForward);
    QTextStream streamBtnBackward(btnBackward);

    tmpValueBtnBack = streamBtnBack.readAll().toInt();
    tmpValueBtnOk = streamBtnOk.readAll().toInt();
    tmpValueBtnForward = streamBtnForward.readAll().toInt();
    tmpValueBtnBackward = streamBtnBackward.readAll().toInt();

    if( valueBtnBack != tmpValueBtnBack ){

#if DEBUG
        qDebug() << "********************************";
        qDebug() << "";
        qDebug() << "Hai letto: ";
        qDebug() << "Back: " << QString::number(tmpValueBtnBack);
        qDebug() << "Ok: " << QString::number(tmpValueBtnOk);
        qDebug() << "Forward: " << QString::number(tmpValueBtnForward);
        qDebug() << "Backward: " << QString::number(tmpValueBtnBackward);
        qDebug() << "";
        qDebug() << "********************************";
#endif

        valueBtnBack = tmpValueBtnBack;
        emettiSegnale(4);

    }
    if( valueBtnOk != tmpValueBtnOk ){

#if DEBUG
        qDebug() << "********************************";
        qDebug() << "";
        qDebug() << "Hai letto: ";
        qDebug() << "Back: " << QString::number(tmpValueBtnBack);
        qDebug() << "Ok: " << QString::number(tmpValueBtnOk);
        qDebug() << "Forward: " << QString::number(tmpValueBtnForward);
        qDebug() << "Backward: " << QString::number(tmpValueBtnBackward);
        qDebug() << "";
        qDebug() << "********************************";
#endif

        valueBtnOk = tmpValueBtnOk;
        emettiSegnale(2);
    }
    if( valueBtnForward != tmpValueBtnForward ){

#if DEBUG
        qDebug() << "********************************";
        qDebug() << "";
        qDebug() << "Hai letto: ";
        qDebug() << "Back: " << QString::number(tmpValueBtnBack);
        qDebug() << "Ok: " << QString::number(tmpValueBtnOk);
        qDebug() << "Forward: " << QString::number(tmpValueBtnForward);
        qDebug() << "Backward: " << QString::number(tmpValueBtnBackward);
        qDebug() << "";
        qDebug() << "********************************";
#endif

        valueBtnForward = tmpValueBtnForward;
        emettiSegnale(3);
    }
    if( valueBtnBackward != tmpValueBtnBackward ){

#if DEBUG
        qDebug() << "********************************";
        qDebug() << "";
        qDebug() << "Hai letto: ";
        qDebug() << "Back: " << QString::number(tmpValueBtnBack);
        qDebug() << "Ok: " << QString::number(tmpValueBtnOk);
        qDebug() << "Forward: " << QString::number(tmpValueBtnForward);
        qDebug() << "Backward: " << QString::number(tmpValueBtnBackward);
        qDebug() << "";
        qDebug() << "********************************";
#endif

        valueBtnBackward = tmpValueBtnBackward;
        emettiSegnale(1);
    }

    btnBack->close();
    btnOk->close();
    btnForward->close();
    btnBackward->close();



    check_timer.singleShot( FREQ_CHECK_TIMER, this, SLOT(checkChangeInBtnsValue()) );
}

void BtnsEventGen::emettiSegnale(int btn){

    switch (btn) {
        case 1:
            if( valueBtnBackward == 1 ){
                timeBackwardPressed = new QElapsedTimer();
                timeBackwardPressed->start();
                emit backwardBtnPressed();
            }
            else if( valueBtnBackward == 0 ){
                int timePressed=timeBackwardPressed->elapsed();
                delete timeBackwardPressed;
                emit backwardBtnReleased(timePressed);
            }

            break;
        case 2:
            if( valueBtnOk== 1 ){
                timeOkPressed = new QElapsedTimer();
                timeOkPressed->start();
                emit okBtnPressed();
            }
            else if( valueBtnOk == 0 ){
                int timePressed=timeOkPressed->elapsed();
                delete timeOkPressed;
                emit okBtnReleased(timePressed);
            }

            break;
        case 3:
            if( valueBtnForward == 1 ){
                timeForwardPressed = new QElapsedTimer();
                timeForwardPressed->start();
                emit forwardBtnPressed();
            }
            else if( valueBtnForward == 0 ){
                int timePressed=timeForwardPressed->elapsed();
                delete timeForwardPressed;
                emit forwardBtnReleased(timePressed);
            }
            break;
        case 4:
            if( valueBtnBack == 1 ){
                timeBackPressed = new QElapsedTimer();
                timeBackPressed->start();
                emit backBtnPressed();
            }
            else if( valueBtnBack == 0 ){
                int timePressed=timeBackPressed->elapsed();
                delete timeBackPressed;
                emit backBtnReleased(timePressed);
            }
            break;

    }
}

void BtnsEventGen::connectSlotsToBtnSignals(int btn, const QObject *receiver, const char *member, bool pressed){

    switch (btn) {

        case 1: //BACKWARD
            if(pressed){
                connect(this, SIGNAL(backwardBtnPressed()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }
            else{
                connect(this, SIGNAL(backwardBtnReleased(int)), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }

            break;

        case 2: //OK
            if(pressed){
                connect(this, SIGNAL(okBtnPressed()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }
            else{
                connect(this, SIGNAL(okBtnReleased(int)), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }

            break;

        case 3: //FORWARD
            if(pressed){
                connect(this, SIGNAL(forwardBtnPressed()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }
            else{
                connect(this, SIGNAL(forwardBtnReleased(int)), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }

            break;

        case 4: //BACK
            if(pressed){
                connect(this, SIGNAL(backBtnPressed()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }
            else{
                connect(this, SIGNAL(backBtnReleased(int)), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            }

            break;


    }
}
