#include "btnsSimulator.h"
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <QDir>
#include <QProcess>
#include "debug.h"

/**
 * @file btnsSimulator.cpp
 * @brief BtnsSimulator class implementation
 */

BtnsSimulator::BtnsSimulator(QString device_file_name, QObject *parent) :
    QObject(parent),
    btns_location("/var/run/VisionAR/BtnsSimulator"),
    btnBack(btns_location+"/input_key4"),
    btnOk(btns_location+"/input_key2"),
    btnForward(btns_location+"/input_key3"),
    btnBackward(btns_location+"/input_key1"),
    lastCode(0),
    tastoPremuto(false), // true alla pressione di un tasto, false al rilascio
    firstLaunch(true)
{
    onConsole("Costruttore BtnsSimulator...");

    // Verifico l'esistenza di /var/run/Eyetechlab e di /var/run/VisionAR/BtnsSimulator
    // e in caso non esistano le creo.
    if( !QDir("/var/run/VisionAR").exists() ){
        QDir().mkdir("/var/run/VisionAR");
    }

    if( !QDir(btns_location).exists() ){
        QDir().mkdir(btns_location);
    }


    // Creo i file rappresentati i tasti e li inizializzo al valore 0
    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnBack);
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnOk);
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnForward);
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnBackward);
    shell.close();


    fd = open(device_file_name.toUtf8().data(), O_RDONLY|O_NONBLOCK);
    if( fd==-1 ){
        qWarning("can not open file");
        return;
    }

    notifier = new QSocketNotifier( fd, QSocketNotifier::Read, this);

    connect( notifier, SIGNAL(activated(int)), this, SLOT(check_keyboard_event(int)) );

}

BtnsSimulator::~BtnsSimulator(){

    if( fd>=0 ){
        close(fd);
    }

    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "rm "+btns_location+"/*");
    shell.close();
    onConsole("Distruttore BtnsSimulator...");
}

void BtnsSimulator::check_keyboard_event(int /* socket */){
    struct input_event buf;
    int code;

    /*
     * Se si lancia da console il programma client di questa libreria,
     *  rimane bufferizzato il rilascio del tasto invio.
     * Con questo if svuoto il buffer.
     */
    if( firstLaunch ){
        firstLaunch = false;
        onConsole("BtnsSimulator::check_keyboard_event() first launch...");
        read(fd,&buf,sizeof(buf));
        read(fd,&buf,sizeof(buf));
        read(fd,&buf,sizeof(buf));
    }


    /*
     * La pressione/rilascio di un tasto sulla tastiera genera i seguenti eventi nel seguente ordine:
     *  1- EV_MSC -tasto premuto/rilasciato
     *  2- EV_KEY -get del valore del tasto premuto
     *  3- EV_SYN
     *
     * Se si preme e rilascia un tasto avremo la generazione dei tre eventi sopra per due volte.
     *
     * Se si preme un tasto e lo si lascia premuto avremo la generazione dei tre eventi sopra,
     * seguita da una continua generazione della sequenza di eventi EV_KEY, EV_SYNC sino al
     * rilascio del tasto, al seguito del quale riviene generata la sequenza EV_MSC, EV_KEY, EV_SYNC
     *
     *  linux/input-event-codes.h per documentarsi su type e code della struct input_event
     *
     */

    if(read(fd,&buf,sizeof(buf))>0){
        if( buf.type == EV_MSC ){ // se viene premuto o rilasciato il tasto


            if( !tastoPremuto ){ // se il tasto viene premuto


                read(fd,&buf,sizeof(buf)); // leggo l'evento EV_KEY
                code =buf.code;
                read(fd,&buf,sizeof(buf));  // leggo l'evento EV_SYN

                parseKeyCode(code);

            }
            else { // se il tasto viene rilasciato o stato premuto un altro tasto mentre è ancora premuto il precedente

                read(fd,&buf,sizeof(buf)); //Leggo l'evento EV_KEY
                code =buf.code;
                read(fd,&buf,sizeof(buf)); //Leggo l'evento EV_SYN

                if( code == lastCode ){ // se il tasto viene rilasciato
                    parseKeyCode(code);

                }else{ // se è stato premuto un altro tasto mentre è ancora premuto il precedente

                }


            }


        }
        else if(tastoPremuto){ // se sta è stati premuto ma non rilasciato il tasto
            //Leggo l'evento EV_SYN
            read(fd,&buf,sizeof(buf));

        }
        else{
            qWarning("BtnsSimulator warning: unexpected event");
        }
    }



}

void BtnsSimulator::parseKeyCode(int code){

    QProcess shell;

    switch (code) {
        case KEY_1:
            if(!tastoPremuto){
                tastoPremuto = true;
                lastCode = code;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+btnBackward);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai leggendo KEY_1: ";
                qDebug() << code;
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }
            else {
                tastoPremuto = false;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnBackward);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai rilasciando KEY_1";
                qDebug() << "";
                qDebug() << "********************************";
#endif

            }

            break;

        case KEY_2:
            if(!tastoPremuto){
                tastoPremuto = true;
                lastCode = code;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+btnOk);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai leggendo KEY_2: ";
                qDebug() << code;
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }
            else{
                tastoPremuto = false;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnOk);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai rilasciando KEY_2";
                qDebug() << "";
                qDebug() << "********************************";
#endif

            }

            break;

        case KEY_3:
            if(!tastoPremuto){
                tastoPremuto = true;
                lastCode = code;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+btnForward);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai leggendo KEY_3: ";
                qDebug() << code;
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }
            else{
                tastoPremuto = false;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnForward);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai rilasciando KEY_3";
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }

            break;

        case KEY_4:
            if(!tastoPremuto){
                tastoPremuto = true;
                lastCode = code;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+btnBack);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai leggendo KEY_4: ";
                qDebug() << code;
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }
            else{
                tastoPremuto = false;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+btnBack);

#if DEBUG
                qDebug() << "********************************";
                qDebug() << "";
                qDebug() << "Stai rilasciando KEY_4";
                qDebug() << "";
                qDebug() << "********************************";
#endif
            }

            break;

    }

    shell.close();
}

QString BtnsSimulator::getBtnsLocation(){
    return btns_location;
}
