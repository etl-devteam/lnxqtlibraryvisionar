#ifndef BTNSSIMULATOR_H
#define BTNSSIMULATOR_H

#include <QObject>
#include <QSocketNotifier>

/**
 * @file btnsSimulator.h
 * @brief Control Unit buttons simulator
 */
class BtnsSimulator : public QObject
{

    Q_OBJECT

    public:
         /**
         * @brief BtnsSimulator constructor
         * @param device_file_name path of the keyboard device file
         * @param parent
         */
        explicit BtnsSimulator(QString device_file_name, QObject *parent = nullptr);

        /**
        * @brief BtnsSimulator distructor
        */
        ~BtnsSimulator();

         /**
         * @brief get method of btns_location field
         * @return the path where the class creates the files simulating
         * the Control Unit buttons device files
         */
        QString getBtnsLocation();

    private:
        QSocketNotifier *notifier;
        QString btns_location;
        QString btnBack;
        QString btnOk;
        QString btnForward;
        QString btnBackward;

        int fd;
        int lastCode;
        bool tastoPremuto;
        bool firstLaunch;

        void parseKeyCode(int code );




    private slots:
        void check_keyboard_event(int socket);
};

#endif // BTNSSIMULATOR_H

