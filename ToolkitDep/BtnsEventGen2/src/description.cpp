/**
* \mainpage Description
*
*  Listens to the Control Unit buttons and generates qt signals if they're pressed or released;
*  specifically, it generates two kinds of signals:
*   - BtnPressed(), when a button is pressed;
*   - BtnReleased(int timePressed), when a button is released; this kind of signals pass also
*   how much time a button was pressed as parameter.
*
* This class offers the method <b>connectSlotsToBtnSignals</b> to connect easly an object slot
* to the wanted signal.
*
* \section connect Exemple of how connect a slot
*
* Assuming you have a class Pippo with a <i>BtnsEventGen* btnEvGen</i> field and these slots as member methods:<br/>
* <i>void  doSomethingWhenBackPressed()</i><br/>
* <i>void  doSomethingWhenBackReleased(int pressedTime)</i><br/>
*
* if you want to connect these slots to the BtnPressed(), BtnReleased(int) signals of the back button, you
* have to add the following instructions in the Pippo's constructor:<br/>
*
*   <i>btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(doSomethingWhenBackPressed()));</i><br/>
*   <i>btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(doSomethingWhenBackReleased(int)), false);</i><br/>
*
*
*
* @note
*   - For design/debugging purpose, you can connect <b>BtnsEventGen</b> to an object of type
*  <b>BtnsSimulator</b> passing the latter as constructor parameter.
*
*   - It isn't required to create a slot for each generated signal: you can choose which event manage.
*
* 
* @author Nicola Pancheri - <nicola.pancheri@akronos-tech.it>
* @date   03/02/2020
* 
*/
