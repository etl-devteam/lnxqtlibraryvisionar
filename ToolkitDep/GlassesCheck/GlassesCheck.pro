QT       += widgets
QT       -= gui

TARGET = GlassesCheck
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += GLASSESCHECK_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/glassesCheck.cpp \
        src/description.cpp

HEADERS += \
        ../../include/VisionAR/ToolkitDep/glassesCheck.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}
