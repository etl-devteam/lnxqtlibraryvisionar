#include "VisionAR/ToolkitDep/glassesCheck.h"
#include <QFile>
#include <QProcess>

GlassesCheck* GlassesCheck::glassesCheck = 0;
const int FREQ_TIMER=20;

GlassesCheck::GlassesCheck(int pid, QObject *parent) :
    QObject(parent),
    pid(pid)
{

    timerCheckGlasses.setInterval(FREQ_TIMER);
    connect(&timerCheckGlasses, SIGNAL(timeout()), this, SLOT(checkGlasses()));
}


GlassesCheck* GlassesCheck::getInstance(int pid){
    if( !glassesCheck ){
        glassesCheck = new GlassesCheck(pid);
    }

    return glassesCheck;
}

void GlassesCheck::cleanInstance(){
    if( glassesCheck )
        delete glassesCheck;
}


void GlassesCheck::checkGlasses(){
    if (!QFile::exists("/sys/class/usbmisc/")){
        QProcess shell;
        shell.execute("/bin/sh", QStringList() << "-c" << "kill -9 "+QString::number(pid));
        shell.close();

    }
}
