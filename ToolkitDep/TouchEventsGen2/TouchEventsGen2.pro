QT       += widgets
QT       -= gui

TARGET = TouchEventsGen2
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += TOUCHEVENTSGEN_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/touchEventsGen.cpp \
        src/description.cpp

HEADERS += \
        ../../include/VisionAR/ToolkitDep/touchEventsGen.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

