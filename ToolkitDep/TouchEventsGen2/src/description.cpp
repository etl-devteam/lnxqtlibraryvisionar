/**
* \mainpage Description
*
* Listens to the touch of the glasses and generate qt signals if notice
* a known gesture. There are 4 recognised gesture:
*   - single tap;
*   - double tap;
*   - swipe forward;
*   - swipe backward.
*
* This class offers the method <b>connectSlotsToTouchSignals</b> to connect easly an object slot
* to the wanted signal.
*
* \section connect Exemple of how connect a slot
*
* Assuming you have a class Pippo with a <i>TouchEventsGen* touchEvGen</i> field and this slot as member method:<br/>
*  <i>void  doSomethingAtSingleTap()</i><br/>
*
* if you want to connect this slot to single tap signal, you
* have to add the following instruction in the Pippo's constructor:<br/>
*
* <i>touchEvGen->connectSlotsToTouchSignals(1, this, SLOT(doSomethingAtSingleTap()));</i><br/>
*
* @note It isn't required to create a slot for each generated signal: you can choose which event manage.
* @author Nicola Pancheri - <nicola.pancheri@akronos-tech.it>
* @date 03/02/2020
* 
*/
