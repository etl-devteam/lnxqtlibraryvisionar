#include "VisionAR/ToolkitDep/touchEventsGen.h"
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <QFile>
#include <QProcess>


TouchEventsGen* TouchEventsGen::touchEvGen = 0;

/**
 * @file touchEventsGen.cpp
 * @brief TouchEventsGen class implementation
 */


TouchEventsGen::TouchEventsGen(QString device_file_name, QObject *parent) :
    QObject(parent),
    tastoPremuto(false)
{
    fd = open(device_file_name.toUtf8().data(), O_RDONLY|O_NONBLOCK);
    if( fd==-1 ){
        qWarning("can not open file");
        return;
    }

    notifier = new QSocketNotifier( fd, QSocketNotifier::Read, this);

    connect( notifier, SIGNAL(activated(int)), this, SLOT(check_touch_event(int)) );
}

TouchEventsGen::~TouchEventsGen(){
    if( fd>=0 ){
        close(fd);

    }

}

TouchEventsGen* TouchEventsGen::getInstance(QString device_file_name){
    if( !touchEvGen )
        touchEvGen = new TouchEventsGen(device_file_name);

    return touchEvGen;

}

void TouchEventsGen::cleanInstance(){
    delete touchEvGen;
}

void TouchEventsGen::check_touch_event(int /* socket */){

    if (!QFile::exists("/sys/class/usbmisc/")){

        QProcess shell;
        shell.execute("/bin/sh", QStringList() << "-c" << "kill -9 $(cat /var/log/VisionAR/pid)");
        shell.close();

    }

    struct input_event buf;
    int code;

    if( !tastoPremuto){
        tastoPremuto = true;

        read(fd,&buf,sizeof(buf));
        code=buf.code;


        if(  code < 63 && code > 58){ //Single Tap

            emit singleTap();

        }
        else if(  code  >=63 && code < 67){ //Double Tap
            emit doubleTap();
        }
        else if(  code  == 67 ){ //Swipe forward
            emit swipeForward();

        }
        else if(  code  == 68 ){ //Swipe backward
            emit swipeBackward();

        }


    }
    else{

            tastoPremuto = false;
            read(fd,&buf,sizeof(buf));

    }

}

void TouchEventsGen::connectSlotsToTouchSignals(int gesture, const QObject *receiver, const char *member){

    switch(gesture){
        case 1: //single tap
            connect(this, SIGNAL(singleTap()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            break;
        case 2: //double tap
            connect(this, SIGNAL(doubleTap()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            break;
        case 3: //swipe avanti
            connect(this, SIGNAL(swipeForward()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            break;
        case 4: //swipe indietro
            connect(this, SIGNAL(swipeBackward()), receiver, member, static_cast<Qt::ConnectionType>(Qt::QueuedConnection));
            break;

    }

}
