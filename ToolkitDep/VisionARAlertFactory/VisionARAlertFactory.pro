QT       += widgets
QT       -= gui

TARGET = VisionARAlertFactory
TEMPLATE = lib

VERSION = 3.0.0

DEFINES += VISIONARALERTFACTORY_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH = ../../include

SOURCES += \
        src/visionARAlertFactory.cpp \
        src/description.cpp \
        src/visionARAlert.cpp \
        src/timedAlert.cpp \
        src/questionAlert.cpp \
        src/alertWithBtns.cpp \
        src/backAlert.cpp \
        src/batteryAlert.cpp

HEADERS += \
        ../../include/VisionAR/ToolkitDep/visionARAlertFactory.h \
        ../../include/VisionAR/ToolkitDep/visionARAlert.h \
        src/timedAlert.h \
        src/questionAlert.h \
        src/alertWithBtns.h \
        src/backAlert.h \
        src/batteryAlert.h

unix {
    target.path = /home/dinex/Library/qt5-embedded/trunk/develop/SKDinema/usr/local/Qt-5.11.1/lib

    INSTALLS += target
}

