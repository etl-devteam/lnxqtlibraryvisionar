#include "alertWithBtns.h"
#include "VisionAR/ToolkitDep/btnsEventGen.h"

BtnsEventGen* btnsEvGen;

AlertWithBtns::AlertWithBtns(QString id, QString msg) :
    VisionARAlert(id, msg),
    stopBtns(false)
{
    btnsEvGen = BtnsEventGen::getInstance();

    btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(backBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(2, this, SLOT(okBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(1, this, SLOT(backwardBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(3, this, SLOT(forwardBtnPressed()));
    btnsEvGen->connectSlotsToBtnSignals(4, this, SLOT(backBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(2, this, SLOT(okBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(1, this, SLOT(backwardBtnReleased(int)), false);
    btnsEvGen->connectSlotsToBtnSignals(3, this, SLOT(forwardBtnReleased(int)), false);

}

AlertWithBtns::~AlertWithBtns(){

}
