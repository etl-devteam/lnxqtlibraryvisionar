#ifndef ALERTWITHBTNS_H
#define ALERTWITHBTNS_H

#include "VisionAR/ToolkitDep/visionARAlert.h"

class AlertWithBtns : public VisionARAlert
{
    Q_OBJECT

    public:
        AlertWithBtns(QString id, QString msg);
        ~AlertWithBtns();

        virtual void showAlert(int time=0) = 0;

    protected:
        bool stopBtns;

    protected slots:
        virtual void backBtnPressed() = 0;
        virtual void backBtnReleased(int timePressed) = 0;
        virtual void okBtnPressed() = 0;
        virtual void okBtnReleased(int timePressed) = 0;
        virtual void forwardBtnPressed() = 0;
        virtual void forwardBtnReleased(int timePressed) = 0;
        virtual void backwardBtnPressed() = 0;
        virtual void backwardBtnReleased(int timePressed) = 0;
};

#endif // ALERTWITHBTNS_H
