#include "backAlert.h"

const int BACK_BTN_DELAY = 3000;
const int borderSpace=20;

BackAlert::BackAlert(QString id, QString msg) :
    AlertWithBtns(id, msg),
    backShown(false),
    backLabel( new QLabel(this))
{
    type = 2;

    msg_label->setStyleSheet("border: 0px;");
    msg_label->setWordWrap(true);
    msg_label->setAlignment(Qt::AlignCenter);
    QRect alertPosition(QPoint(), QSize(this->width()-(borderSpace*2), (this->height()-borderSpace)/2));
    alertPosition.moveTopLeft(this->rect().topLeft() += QPoint(10, 10));
    msg_label->setGeometry(alertPosition);

    backLabel->setText("Press back");
    backLabel->setStyleSheet("border: 0px solid "+coloreVisibile+";");
    backLabel->setWordWrap(true);
    backLabel->setAlignment(Qt::AlignCenter);
    backLabel->resize( 80, 20);
    QRect noPosition(QPoint(), QSize(80, 20));
    noPosition.moveBottomRight(this->rect().bottomRight() += QPoint(-10, -5));
    backLabel->setGeometry(noPosition);
    backLabel->hide();

}

BackAlert::~BackAlert(){
    delete backLabel;
}

void BackAlert::showAlert(int /* time */){

    timerShowBack.singleShot(BACK_BTN_DELAY, this, SLOT(showBackBtn()));
    this->show();
}

void BackAlert::backBtnPressed(){
    if( !stopBtns ){
        if( backShown ){
            stopBtns = true;
            emit hideAlert(true);

        }
     }
}

void BackAlert::showBackBtn(){
    backLabel->show();
    backShown = true;
}
