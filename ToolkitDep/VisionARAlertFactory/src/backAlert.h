#ifndef BACKALERT_H
#define BACKALERT_H

#include "alertWithBtns.h"
#include <QTimer>

class BackAlert : public AlertWithBtns
{
    Q_OBJECT

    public:
        BackAlert(QString id, QString msg);
        ~BackAlert();

        virtual void showAlert(int time);

    private:
        bool backShown;
        QLabel *backLabel;
        QTimer timerShowBack;


    private slots:
        virtual void backBtnPressed();
        virtual void backBtnReleased(int /* timePressed */){}
        virtual void okBtnPressed(){}
        virtual void okBtnReleased(int /* timePressed */){}
        virtual void forwardBtnPressed(){}
        virtual void forwardBtnReleased(int /* timePressed */){}
        virtual void backwardBtnPressed(){}
        virtual void backwardBtnReleased(int /* timePressed */){}

        void showBackBtn();
};

#endif // BACKALERT_H
