#include "batteryAlert.h"
#include <QFile>
#include <QTextStream>


const QString LOCAL_BATTERY="/sys/class/dinema/local_battery";
const int FREQ_TIMER = 100;

BatteryAlert::BatteryAlert(QString id, QString msg) :
    VisionARAlert(id, msg)
{
    type = -1;

    QRect alertPosition(QPoint(), QSize( this->width(), this->height()));
    alertPosition.moveCenter(this->rect().center());
    msg_label->setGeometry(alertPosition);
    msg_label->setWordWrap(true);
    msg_label->setAlignment(Qt::AlignCenter);
}


void BatteryAlert::showAlert(int){
    timerCheckBattery.singleShot(FREQ_TIMER, this, SLOT(checkBattery()));
    this->show();
}


void BatteryAlert::checkBattery(){
    QFile battery(LOCAL_BATTERY);

    battery.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream stream(&battery);

    int localBatteryVal = stream.readAll().toInt();

    battery.close();

    if(localBatteryVal == 1){ //la batteria non è stata ancora cambiata
        timerCheckBattery.singleShot(FREQ_TIMER, this, SLOT(checkBattery()));
    }
    else{ // la batteria è stata cambiata
        emit hideAlert(true);
    }
}
