#ifndef BATTERYALERT_H
#define BATTERYALERT_H

#include "VisionAR/ToolkitDep/visionARAlert.h"
#include <QTimer>


class BatteryAlert : public VisionARAlert
{
    Q_OBJECT

    public:
        BatteryAlert(QString id, QString msg);

        virtual void showAlert(int time=0);

    private:
        QTimer timerCheckBattery;

    private slots:
        void checkBattery();

};

#endif // BATTERYALERT_H
