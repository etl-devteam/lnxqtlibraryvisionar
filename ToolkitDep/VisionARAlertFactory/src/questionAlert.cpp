#include "questionAlert.h"

const int borderSpace=20;

QuestionAlert::QuestionAlert(QString id, QString msg) :
    AlertWithBtns(id, msg),
    yesSelected(true),
    yesLabel(new QLabel(this)),
    noLabel(new QLabel(this))
{
    type = 1;

    msg_label->setStyleSheet("border: 0px;");
    msg_label->setWordWrap(true);
    msg_label->setAlignment(Qt::AlignCenter);
    QRect alertPosition(QPoint(), QSize(this->width()-(borderSpace*2), (this->height()-borderSpace)/2));
    alertPosition.moveTopLeft(this->rect().topLeft() += QPoint(10, 10));
    msg_label->setGeometry(alertPosition);

    yesLabel->setText("Yes");
    yesLabel->setStyleSheet("color: black; background: "+coloreVisibile+";border: 1px solid "+coloreVisibile+";");
    yesLabel->setWordWrap(true);
    yesLabel->setAlignment(Qt::AlignCenter);
    yesLabel->resize( 100, 20);
    QRect yesPosition(QPoint(), QSize(100, 20));
    yesPosition.moveBottomRight(this->rect().bottomRight() += QPoint(-this->width()/2-15, -10));
    yesLabel->setGeometry(yesPosition);

    noLabel->setText("No");
    noLabel->setStyleSheet("border: 1px solid "+coloreVisibile+";");
    noLabel->setWordWrap(true);
    noLabel->setAlignment(Qt::AlignCenter);
    noLabel->resize( 100, 20);
    QRect noPosition(QPoint(), QSize(100, 20));
    noPosition.moveBottomLeft(this->rect().bottomLeft() += QPoint(this->width()/2+15, -10));
    noLabel->setGeometry(noPosition);

}

QuestionAlert::~QuestionAlert(){

    delete yesLabel;
    delete noLabel;

}

void QuestionAlert::showAlert(int /* time */){
    this->show();
}

void QuestionAlert::okBtnPressed(){
    if( !stopBtns ){
        stopBtns = true;
        emit hideAlert(yesSelected);

    }
}

void QuestionAlert::forwardBtnPressed(){

    if( !stopBtns ){
        if( yesSelected ){
            yesSelected = false;
            noLabel->setStyleSheet("color: black; background: "+coloreVisibile+"; border: 1px solid "+coloreVisibile+";");
            yesLabel->setStyleSheet("color: "+coloreVisibile+"; background: black; border: 1px solid "+coloreVisibile+";");
        }
    }

}

void QuestionAlert::backwardBtnPressed(){

    if( !stopBtns ){
        if( !yesSelected ){
            yesSelected = true;
            yesLabel->setStyleSheet("color: black; background: "+coloreVisibile+"; border: 1px solid "+coloreVisibile+";");
            noLabel->setStyleSheet("color: "+coloreVisibile+"; background: black; border: 1px solid "+coloreVisibile+";");
        }
    }

}
