#ifndef QUESTIONALERT_H
#define QUESTIONALERT_H

#include "alertWithBtns.h"

class QuestionAlert : public AlertWithBtns
{
    Q_OBJECT

    public:
        QuestionAlert(QString id, QString msg);
        ~QuestionAlert();

        virtual void showAlert(int time);

    private:
        bool yesSelected;
        QLabel *yesLabel;
        QLabel *noLabel;

    private slots:
        virtual void backBtnPressed(){}
        virtual void backBtnReleased(int /* timePressed */){}
        virtual void okBtnPressed();
        virtual void okBtnReleased(int /* timePressed */){}
        virtual void forwardBtnPressed();
        virtual void forwardBtnReleased(int /* timePressed */){}
        virtual void backwardBtnPressed();
        virtual void backwardBtnReleased(int /* timePressed */){}

};

#endif // QUESTIONALERT_H
