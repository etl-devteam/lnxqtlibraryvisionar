#include "timedAlert.h"

TimedAlert::TimedAlert(QString id, QString msg) :
    VisionARAlert(id, msg)
{
    type = 0;

    QRect alertPosition(QPoint(), QSize( this->width(), this->height()));
    alertPosition.moveCenter(this->rect().center());
    msg_label->setGeometry(alertPosition);
    msg_label->setWordWrap(true);
    msg_label->setAlignment(Qt::AlignCenter);

}

void TimedAlert::showAlert(int time){
    timer_hide_msg.singleShot(time, this, &TimedAlert::time_elapsed);
    this->show();

}

void TimedAlert::time_elapsed(){

    emit hideAlert(true);

}

