#ifndef TIMEDALERT_H
#define TIMEDALERT_H

#include "VisionAR/ToolkitDep/visionARAlert.h"
#include <QTimer>

class TimedAlert : public VisionARAlert
{
    Q_OBJECT

    public:
        TimedAlert(QString id, QString msg);

        virtual void showAlert(int time=5000);

    private:
        QTimer timer_hide_msg;

    private slots:
        void time_elapsed();
};

#endif // TIMEDALERT_H
