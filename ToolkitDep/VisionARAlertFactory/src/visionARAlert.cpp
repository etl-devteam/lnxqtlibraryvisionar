#include "VisionAR/ToolkitDep/visionARAlert.h"

VisionARAlert::VisionARAlert(QString id, QString msg, QWidget *parent) :
    QWidget(parent),
    msg_label( new QLabel(this) ),
    id(id),
    coloreVisibile("#7fff00")
{
    this->hide();
    msg_label->setText(msg);
    this->setStyleSheet("border: 1px solid "+coloreVisibile+";");
    this->setFixedSize(413, 133);

}

VisionARAlert::~VisionARAlert(){
   this->hide();
   delete msg_label;
}

QString VisionARAlert::getId(){
    return id;
}

QString VisionARAlert::getMsg(){
    return msg_label->text();
}

int VisionARAlert::getType(){
    return type;
}
