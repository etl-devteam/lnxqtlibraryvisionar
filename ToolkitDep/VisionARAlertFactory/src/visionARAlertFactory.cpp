#include "VisionAR/ToolkitDep/visionARAlertFactory.h"
#include "timedAlert.h"
#include "questionAlert.h"
#include "backAlert.h"
#include "batteryAlert.h"

VisionARAlertFactory* VisionARAlertFactory::alertsFactory = 0;

VisionARAlertFactory::VisionARAlertFactory(QObject *parent) :
    QObject(parent),
    minAlertType(0),
    maxAlertType(2)
{

}


VisionARAlertFactory* VisionARAlertFactory::getInstance(){

    if( !alertsFactory ){
        alertsFactory = new VisionARAlertFactory();
    }

    return alertsFactory;
}

void VisionARAlertFactory::cleanInstance(){
    if(alertsFactory)
        delete alertsFactory;
}

VisionARAlert* VisionARAlertFactory::getAlert(int type, QString id, QString msg){

    VisionARAlert *toRet = nullptr;

    switch (type) {
        case -1:
            toRet = new BatteryAlert(id, msg);
            break;

        case 0:
            toRet = new TimedAlert(id, msg);
            break;

        case 1:
            toRet = new QuestionAlert(id, msg);
            break;

        case 2:
            toRet = new BackAlert(id, msg);
            break;
    }

    return toRet;
}

int VisionARAlertFactory::getMinAlertType(){
    return minAlertType;
}

int VisionARAlertFactory::getMaxAlertType(){
    return maxAlertType;
}
