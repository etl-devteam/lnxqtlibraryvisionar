#ifndef ABSTRACTVISIONARCONTROLLER_H
#define ABSTRACTVISIONARCONTROLLER_H

#include "VisionAR/Framework/abstractvisionarcontroller_global.h"
#include "VisionAR/Toolkit/threadObjInterface.h"
#include "VisionAR/Toolkit/launcherAdapterInterface.h"
#include "VisionAR/Framework/abstractVisionARView.h"
#include "VisionAR/Framework/abstractVisionARModel.h"


class ABSTRACTVISIONARCONTROLLERSHARED_EXPORT AbstractVisionARController :
        public QObject,
        public LauncherAdapterInterface,
        public ThreadObjInterface
{
	Q_OBJECT

	public:
        AbstractVisionARController(AbstractVisionARView* view, AbstractVisionARModel* model, QString app_relative_path=nullptr);
        ~AbstractVisionARController();

    protected:
        AbstractVisionARModel* model;
        AbstractVisionARView* view;

        virtual void connections() = 0;
        virtual void endActions() = 0;

   private slots:
        virtual void start();
        void end();

    private:
        void defaultConnections();
};

#endif // ABSTRACTVISIONARCONTROLLER_H

