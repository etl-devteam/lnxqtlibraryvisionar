#ifndef ABSTRACTVISIONARMODEL_H
#define ABSTRACTVISIONARMODEL_H

#include "abstractvisionarmodel_global.h"
#include <QObject>
#include <VisionAR/Toolkit/threadObjInterface.h>
#include <VisionAR/Toolkit/btnsInterface.h>
#include <VisionAR/Toolkit/touchInterface.h>
#include <VisionAR/ToolkitDep/visionARAlert.h>


class ABSTRACTVISIONARMODELSHARED_EXPORT AbstractVisionARModel :
        public QObject,
        public ThreadObjInterface,
        public BtnsInterface,
        public TouchInterface
{
	Q_OBJECT

public:
    explicit AbstractVisionARModel(QObject *parent = nullptr);
    ~AbstractVisionARModel() override;

    virtual void runModel() = 0;

protected:
    virtual void backPressed()                           = 0;
    virtual void backReleased(int timePressed)           = 0;
    virtual void okPressed()                             = 0;
    virtual void okReleased(int timePressed)             = 0;
    virtual void forwardPressed()                        = 0;
    virtual void forwardReleased(int timePressed)        = 0;
    virtual void backwardPressed()                       = 0;
    virtual void backwardReleased(int timePressed)       = 0;
    virtual void singleTap()                             = 0;
    virtual void doubleTap()                             = 0;
    virtual void swipeForward()                          = 0;
    virtual void swipeBackward()                         = 0;
    virtual void manageAlertsEnd(QString id, bool value) = 0;

signals:
    void showWindow( int );
    void hideActiveWindow();
    void showActiveWindow();
    void newAlert(int type, QString id, QString msg, bool priority=false);
    void endApplication();

private:
    VisionARAlert* activeAlert;

private slots:
     void backBtnPressed()                     override;
     void backBtnReleased(int timePressed)     override;
     void okBtnPressed()                       override;
     void okBtnReleased(int timePressed)       override;
     void forwardBtnPressed()                  override;
     void forwardBtnReleased(int timePressed)  override;
     void backwardBtnPressed()                 override;
     void backwardBtnReleased(int timePressed) override;
     void touchSingleTap()                     override;
     void touchDoubleTap()                     override;
     void touchSwipeForward()                  override;
     void touchSwipeBackward()                 override;

     void start();
     void showAlert(QWidget *alert);
     void hideAlert(QString id, bool value);
};

#endif // ABSTRACTVISIONARMODEL_H

