#ifndef ABSTRACTVISIONARVIEW_H
#define ABSTRACTVISIONARVIEW_H

#include "abstractvisionarview_global.h"
#include <QMainWindow>
#include <QLabel>
#include <VisionAR/Toolkit/threadObjInterface.h>


class ABSTRACTVISIONARVIEWSHARED_EXPORT AbstractVisionARView :
        public QMainWindow,
        public ThreadObjInterface
{

	Q_OBJECT

	public:
        explicit AbstractVisionARView(QWidget *parent = nullptr);
        ~AbstractVisionARView();

    private:
        QLabel batteryLabel;

    protected:
        QList<QWidget *> windows;
        QList<bool> showingBattery;
        int activeWindow;
        QWidget *alert;
        const QString coloreVisibile;

        template<class T>
        void registerWindow(T* obj, bool showBattery=false){

            windows.append(new QWidget());
            ((T*)obj)->setupUi(windows.last());

            showingBattery.append(showBattery);

        }

        QList<QPixmap*> batteryIcons;



    public slots:
        virtual void start(){}
        void showWindow( int num );
        void hideActiveWindow();
        void showActiveWindow();

    private slots:
        void showAlert(QWidget *alert);
        void hideAlert(bool);
        void showingPriorityAlert();


};

#endif // ABSTRACTVISIONARVIEW_H

