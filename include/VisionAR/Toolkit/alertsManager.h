#ifndef ALERTSMANAGER_H
#define ALERTSMANAGER_H

#include "VisionAR/Toolkit/alertsmanager_global.h"
#include <QObject>
#include <QTimer>
#include <QQueue>
#include "VisionAR/ToolkitDep/visionARAlert.h"


class ALERTSMANAGERSHARED_EXPORT AlertsManager : public QObject
{

	Q_OBJECT

    public:
        static AlertsManager* getInstance();
        static void cleanInstance();

    private:
		explicit AlertsManager(QObject *parent = nullptr);
        ~AlertsManager();

        class AlertToRemember{


            public:
                AlertToRemember(int type, QString id, QString msg) :
                    type(type),
                    id(id),
                    msg(msg)
                {

                }

                QString getId(){ return id; }
                QString getMsg(){ return msg; }
                int getType(){ return type; }

            private:
                int type;
                QString id;
                QString msg;

        };

        static AlertsManager* alertsMng;
        QQueue<VisionARAlert*> low_priority_buffer;
        QQueue<VisionARAlert*> high_priority_buffer;
        QTimer timer_check_low_priority_buffer;
        QTimer timer_check_high_priority_buffer;
        QTimer timer_check_external_buffer;
        bool showingAlertLowPriority;
        bool showingAlertHighPriority;
        AlertToRemember* actualLowPriorityAlert;

        void parse_xml_msg(QString filename);

    private slots:
        void check_alerts_in_low_priority_buffer();
        void check_alerts_in_high_priority_buffer();
        void check_alerts_in_external_buffer();

    public slots:
        /**
        * @brief Used to insert messages in one of the two queues ( priority and no-priority )
        * @param type the type of message inserted ( from 0 to 2 )
        * @param msg the message to show
        * @param time the time the messagge will be showed off ( valid only for type == 0 )
        * @param priority set if the message will be inserted in the priority queue or in the no-priority queue.
        */
        void addAlert(int type, QString id, QString msg, bool priority=false); //priority == false low_priority_buffer else high_priority_buffer
        void alertHidden(QString id, bool value);

    signals:
        void showAlert(QWidget* alert);
        /*
         * Il parametro value è usato da question alert per
         * dire che bottone è stato selezionato (yes o no ) e quindi intrapendere
         * l'adeguata azione
         */
        void hideAlert(QString id, bool value);

        void showingPriorityAlert();

};

#endif // ALERTSMANAGER_H

