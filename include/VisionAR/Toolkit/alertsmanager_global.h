#ifndef ALERTSMANAGER_GLOBAL_H
#define ALERTSMANAGER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ALERTSMANAGER_LIBRARY)
#  define ALERTSMANAGERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ALERTSMANAGERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ALERTSMANAGER_GLOBAL_H

