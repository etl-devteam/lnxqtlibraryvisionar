#ifndef BATTERYMANAGER_H
#define BATTERYMANAGER_H

#include "batterymanager_global.h"
#include <QObject>
#include <QTimer>

class BATTERYMANAGERSHARED_EXPORT BatteryManager : public QObject
{

	Q_OBJECT


	public:
        static BatteryManager* getInstance();
        static void cleanInstance();

        QString getIdQuestionAlert();
        QString getIdSwapAlert();


    private:
        explicit BatteryManager(QObject *parent = nullptr);
        ~BatteryManager();

        static BatteryManager* batteryMng;
        QTimer *timer;
        bool timer_elapsed;
        QString idQuestionAlert;
        QString idSwapAlert;


    private slots:
        void backBtnPressed();
        void backBtnReleased(int timePressed);

        void startCheckSwitchBattery();
        void hideAlert(QString id, bool value);

    signals:
        void newAlert(int type, QString id, QString msg, bool priority=true);

};

#endif // BATTERYMANAGER_H

