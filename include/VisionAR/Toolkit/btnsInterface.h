#ifndef BTNSINTERFACE_H
#define BTNSINTERFACE_H

#include <QObject>

class BtnsInterface
{

	public:
        BtnsInterface();
        virtual ~BtnsInterface();

        /**
        * @brief connects the slot passed as parameter to the choosen button signal.
        * @param btn the choosen button signal to connect to. It can have a value from 1 to 4 with this meaning:
        *  - 1 backward
        *  - 2 ok
        *  - 3 forward
        *  - 4 back
        * @param receiver the object which has the slot parameter <b>member</b> as method
        * @param member the slot to connect to the button signal
        * @param pressed if true connect the slot <b>member</b> to the <i>pressed signal</i> of <b>btn</b>
        *  else connect <b>member</b> to the <i>released signal</i>
        */
       void connectSlotsToBtnSignals( int btn, const QObject *receiver, const char *member, bool pressed=true );
       void init(QObject *parent);

       virtual void backBtnPressed() = 0;
       virtual void backBtnReleased(int timePressed) = 0;
       virtual void okBtnPressed() = 0;
       virtual void okBtnReleased(int timePressed) = 0;
       virtual void forwardBtnPressed() = 0;
       virtual void forwardBtnReleased(int timePressed) = 0;
       virtual void backwardBtnPressed() = 0;
       virtual void backwardBtnReleased(int timePressed) = 0;

};

#endif // BTNSINTERFACE_H

