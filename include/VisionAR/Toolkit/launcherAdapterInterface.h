#ifndef LAUNCHERADAPTERINTERFACE_H
#define LAUNCHERADAPTERINTERFACE_H

#include <QApplication>
#include <QTimer>


class LauncherAdapterInterface
{
public:
    LauncherAdapterInterface(QString app_relative_path = nullptr);
    virtual ~LauncherAdapterInterface();

    static void startCheckingGlasses();

protected:
    virtual void endActions() = 0;
    void endApplication();
};

#endif // LAUNCHERADAPTERINTERFACE_H

