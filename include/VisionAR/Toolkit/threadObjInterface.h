#ifndef THREADOBJINTERFACE_H
#define THREADOBJINTERFACE_H

#include <QThread>

class ThreadObjInterface
{

	public:
        ThreadObjInterface();
        virtual ~ThreadObjInterface();

        void init(QObject *superObj);
        void runThread();
        void stopThread();

    protected:
        QThread thread;

};

#endif // THREADOBJINTERFACE_H

