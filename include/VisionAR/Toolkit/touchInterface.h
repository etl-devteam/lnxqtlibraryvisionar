#ifndef TOUCHINTERFACE_H
#define TOUCHINTERFACE_H

#include <QObject>

class TouchInterface
{

	public:
        TouchInterface();
        virtual ~TouchInterface();

        /**
        * @brief connects the slot passed as parameter to the choosen gesture signal.
        * @param gesture the kind of gesture signal to connect to the slot passed as parameter;
        *         It can have a value from 1 to 4 with this meaning:
        *      - 1 single tap
        *      - 2 double tap
        *      - 3 swipe forward
        *      - 4 swipe backward
        * @param receiver the object which has the slot parameter <b>member</b> as method
        * @param member the slot to connect to the gesture signal
        */
       void connectSlotsToTouchSignals( int gesture, const QObject *receiver, const char *member );
       void init(QObject *parent);

       virtual void touchSingleTap() = 0;
       virtual void touchDoubleTap() = 0;
       virtual void touchSwipeForward() = 0;
       virtual void touchSwipeBackward() = 0;

};

#endif // TOUCHINTERFACE_H

