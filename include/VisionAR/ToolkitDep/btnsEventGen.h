#ifndef BTNSEVENTGEN_H
#define BTNSEVENTGEN_H

#include "VisionAR/ToolkitDep/btnseventgen_global.h"
#include <QObject>
#include <QFile>
#include <QElapsedTimer>
#include <QTimer>


/**
 * @file btnsEventGen.h
 * @brief Control Unit buttons events generator
 */

class BTNSEVENTGENSHARED_EXPORT BtnsEventGen : public QObject
{

    Q_OBJECT

    public:


        /*
         * Metodo usato per connettere una slot ai segnali pressed/released di un tasto
         * della Control Unit.
         *
         * Parametri:
         *  - int btn: a quale bottone ci si vuole connettere;
         *          1 backward
         *          2 ok
         *          3 forward
         *          4 back
         *  - const QObject *receiver: l'oggetto con la slot che si vuole connettere;
         *  - const char *member: la slot di receiver passata tramite la macro SLOT();
         *  - bool pressed: true connette la slot al segnale pressed del bottone specificato,
         *                  false connette la slot al segnale released del bottone specificato;
         *
         */

         /**
         * @brief connects the slot passed as parameter to the choosen button signal.
         * @param btn the choosen button signal to connect to. It can have a value from 1 to 4 with this meaning:
         *  - 1 backward
         *  - 2 ok
         *  - 3 forward
         *  - 4 back
         * @param receiver the object which has the slot parameter <b>member</b> as method
         * @param member the slot to connect to the button signal
         * @param pressed if true connect the slot <b>member</b> to the <i>pressed signal</i> of <b>btn</b>
         *  else connect <b>member</b> to the <i>released signal</i>
         */
        void connectSlotsToBtnSignals( int btn, const QObject *receiver, const char *member, bool pressed=true );

        static BtnsEventGen* getInstance();
        static void cleanInstance();

    private:

        /**
        * @brief BtnsEventGen constructor
        * @param btnsSim pointer to a BtnsSimulator object; you have to pass it only in debugging phase
        *        if you want to simulate Control Unit buttons on the host machine through the keyboard.
        *
        * @param parent
        */
       explicit BtnsEventGen(QObject *parent = nullptr);
       ~BtnsEventGen();

        static BtnsEventGen* btnsEvGen;

        QTimer check_timer;
        QString path_btns_dir;

        QFile *btnBack;
        QFile *btnOk;
        QFile *btnForward;
        QFile *btnBackward;
        QElapsedTimer *timeBackPressed;
        QElapsedTimer *timeOkPressed;
        QElapsedTimer *timeForwardPressed;
        QElapsedTimer *timeBackwardPressed;
        int valueBtnBack;
        int valueBtnOk;
        int valueBtnForward;
        int valueBtnBackward;

        void emettiSegnale( int btn );

    private slots:
        void checkChangeInBtnsValue();

    signals:
        /**
         * @brief emitted when the back button is pressed.
         */
        void backBtnPressed();

        /**
         * @brief emitted when the back button is released.
         * @param timePressed how much time back was pressed
         */
        void backBtnReleased(int timePressed);

        /**
         * @brief emitted when the ok button is pressed.
         */
        void okBtnPressed();

        /**
         * @brief emitted when the ok button is released.
         * @param timePressed how much time back was pressed
         */
        void okBtnReleased(int timePressed);

        /**
         * @brief emitted when the forward button is pressed.
         */
        void forwardBtnPressed();

        /**
         * @brief emitted when the forward button is released.
         * @param timePressed how much time back was pressed
         */
        void forwardBtnReleased(int timePressed);

        /**
         * @brief emitted when the backward button is pressed.
         */
        void backwardBtnPressed();

        /**
         * @brief emitted when the backward button is released.
         * @param timePressed how much time back was pressed
         */
        void backwardBtnReleased(int timePressed);

};

#endif // BTNSEVENTGEN_H

