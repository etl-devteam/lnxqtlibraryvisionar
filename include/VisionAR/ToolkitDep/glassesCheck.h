#ifndef GLASSESCHECK_H
#define GLASSESCHECK_H

#include "VisionAR/ToolkitDep/glassescheck_global.h"
#include <QObject>
#include <QTimer>

class GLASSESCHECKSHARED_EXPORT GlassesCheck : public QObject
{

	Q_OBJECT

	public:
        static GlassesCheck* getInstance(int pid = 999999);
        static void cleanInstance();

    private:
        explicit GlassesCheck(int pid, QObject *parent = nullptr);

        static GlassesCheck* glassesCheck;
        QTimer timerCheckGlasses;
        int pid;

    private slots:
        void checkGlasses(); //controlla che gli occhiali siano attaccati



};

#endif // GLASSESCHECK_H

