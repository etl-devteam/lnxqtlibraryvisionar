#ifndef TOUCHEVENTSGEN_H
#define TOUCHEVENTSGEN_H

#include "toucheventsgen_global.h"
#include <QObject>
#include <QSocketNotifier>

/**
 * @file touchEventsGen.h
 * @brief VisionAR glasses touch events generator
 */


class TOUCHEVENTSGENSHARED_EXPORT TouchEventsGen : public QObject
{

    Q_OBJECT

    public:


         /**
         * @brief connects the slot passed as parameter to the choosen gesture signal.
         * @param gesture the kind of gesture signal to connect to the slot passed as parameter;
         *         It can have a value from 1 to 4 with this meaning:
         *      - 1 single tap
         *      - 2 double tap
         *      - 3 swipe forward
         *      - 4 swipe backward
         * @param receiver the object which has the slot parameter <b>member</b> as method
         * @param member the slot to connect to the gesture signal
         */
        void connectSlotsToTouchSignals( int gesture, const QObject *receiver, const char *member );

        static TouchEventsGen* getInstance(QString device_file_name);
        static void cleanInstance();

    private:
        /**
         * @brief TouchEventsGen constructor
         * @param device_file_name the device file of the touch device; it should be "/dev/input/event0"
         * @param parent
         */
        explicit TouchEventsGen(QString device_file_name, QObject *parent = nullptr);
        ~TouchEventsGen();

        static TouchEventsGen* touchEvGen;

        QSocketNotifier *notifier;
        int fd;
        bool tastoPremuto;

    private slots:
        void check_touch_event(int socket);

    signals:
        /**
         * @brief emitted when a user do a single tap on touch
         */
        void singleTap();

        /**
         * @brief emitted when a user do a double tap on touch
         */
        void doubleTap();

        /**
         * @brief emitted when a user do a swipe forward on touch
         */
        void swipeForward();

        /**
         * @brief emitted when a user do a swipe backward on touch
         */
        void swipeBackward();


};

#endif // TOUCHEVENTSGEN_H

