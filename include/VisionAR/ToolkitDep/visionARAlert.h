#ifndef VISIONARALERT_H
#define VISIONARALERT_H

#include <QWidget>
#include <QLabel>

class VisionARAlert : public QWidget
{
    Q_OBJECT
    public:
        explicit VisionARAlert(QString id, QString msg, QWidget *parent = nullptr);
        ~VisionARAlert();

        virtual void showAlert(int time=0) = 0;
        QString getId();
        QString getMsg();
        int getType();


    protected:
        QLabel* msg_label;
        QString id;
        int type;
        const QString coloreVisibile;


    signals:
        /*
         * Il parametro value è usato da question alert per
         * dire che bottone è stato selezionato (yes o no ) e quindi intrapendere
         * l'adeguata azione
         */
        void hideAlert(bool value);
};

#endif // VISIONARALERT_H
