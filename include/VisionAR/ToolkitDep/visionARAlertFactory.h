#ifndef VISIONARALERTFACTORY_H
#define VISIONARALERTFACTORY_H

#include "VisionAR/ToolkitDep/visionaralertfactory_global.h"
#include <QObject>
#include "VisionAR/ToolkitDep/visionARAlert.h"

class VISIONARALERTFACTORYSHARED_EXPORT VisionARAlertFactory : public QObject
{

	Q_OBJECT

	public:

        static VisionARAlertFactory* getInstance();
        static void cleanInstance();

        VisionARAlert* getAlert(int type, QString id, QString msg);
        int getMinAlertType();
        int getMaxAlertType();

    private:
		explicit VisionARAlertFactory(QObject *parent = nullptr);
        static VisionARAlertFactory* alertsFactory;
        int minAlertType;
        int maxAlertType;
};

#endif // VISIONARALERTFACTORY_H

