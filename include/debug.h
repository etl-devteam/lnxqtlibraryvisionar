#ifndef DEBUG_EYETECHLIB
#define DEBUG_EYETECHLIB

#define DEBUG false 

#include <QString>

#if DEBUG

#include <QDebug>
#include <QThread>

static void onConsole(QString msg){
	qDebug() << "Thread " << QThread::currentThreadId() << ": " <<  msg;
}
#else
static void onConsole(QString /* msg */){}
#endif

#endif
