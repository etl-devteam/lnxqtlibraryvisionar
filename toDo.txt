- Aggiungere emissione di eccezione nel caso si faccia una newAlert con un
  type inesistente

- Aggiungere emissione di eccezione nel caso si faccia una newAlert con id
  gia esistente

- In AlertManager far si che si possa impostare il tempo nei messaggi
  temporizzati  esterni

-In AlertManager occhio che se usato solo come toolkit e non all'interno del
framework, il metodo hiddenMessage o qualcosa di simile risulta macchinoso.
ricontrollare e riprogettare!
